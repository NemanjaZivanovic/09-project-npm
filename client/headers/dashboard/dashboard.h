#ifndef DASHBOARD_H
#define DASHBOARD_H

#include <QGraphicsScene>
#include <QImage>

class Dashboard
{
public:
    Dashboard(int width, int height, QImage background, QGraphicsScene* scene);

    void setDashboard() const;

private:
    // Dimenzije scene
    qreal m_width;
    qreal m_height;

    // Broj igara u listi, potreban za definisanje dimenzija
    const size_t m_gameNum = 6;
    // Stranica neuvecane slike od igre u listi
    const qreal m_gameItemSize = 200;

    QGraphicsScene* m_scene;
    QImage m_background;

};

#endif // DASHBOARD_H
