#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include <QWidget>

namespace Ui {
class Notifications;
}

class Notifications : public QWidget
{
    Q_OBJECT

public:
    explicit Notifications(QWidget *parent = nullptr);
    ~Notifications();

    void setMessage(QString);

private:
    Ui::Notifications *ui;
};

#endif // NOTIFICATIONS_H
