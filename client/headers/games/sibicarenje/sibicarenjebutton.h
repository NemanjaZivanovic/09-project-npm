#ifndef SIBICARENJEBUTTON_H
#define SIBICARENJEBUTTON_H

#include <QGraphicsObject>

class SibicarenjeButton: public QGraphicsObject
{
    Q_OBJECT
public:
    SibicarenjeButton(qint16, qint16, qint16, qint16, QString text);

signals:
    void clicked();

public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void setText(QString text);

    int type() const override;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QRectF m_boundingRect;
    QString m_text;
    qint16 m_x, m_y, m_width, m_height;
};

#endif // SIBICARENJEBUTTON_H
