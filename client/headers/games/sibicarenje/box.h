#ifndef BOX_H
#define BOX_H

#include "headers/games/sibicarenje/currentbox.h"

#include <QGraphicsObject>

#define NONE 0
#define RIGHT_SIDE 1
#define LEFT_SIDE 2

class Box: public QGraphicsObject
{
    Q_OBJECT
public:
    Box(CurrentBox);

signals:
    // signal koji se salje kada treba da se otvori kutija koja sadrzi lopticu
    // kada se emituje ovaj signal, u klasi sibicarenje se poziva slot showBall
    // koji onda prikazuje skrivenu lopticu
    void showBall();
    // isto kao showBall samo za skrivanje loptice
    void hideBall();
    // signal koji postavlja m_canOpenBox na odgovarajucu vrednost svim kutijama
    // sluzi da blokira ili odblokira otvaranje kutija, tj mousePressEvent iz ove klase
    // ce moci da nastavi put ako je kutiji dozvoljeno da bude otvorena
    void sendBlockOpeningBoxSignal(bool);
    // signalizira da je rotacija date kutije zavrsena, sluzice funkciji prepareNewRotation u
    // klasi Sibicarenje
    void finishedRotating();
    // kada se odabere odredjena kutija, ona emituje signal sa vrednoscu CurrentBox trenutne kutije
    void chosenBox(CurrentBox);

public slots:
    // blokira/odblokira otvaranje trenutne kutije
    void blockOpeningBox(bool);
    // iz klase Sibicarenje se poziva ovaj slot, sustina je da se kaze kutiji da ona treba da pokrene
    // rotaciju na odredjenu stranu, a brzina je odredjena trecim argumentom (nivoom)
    void prepareNewRotation(CurrentBox box, qint16 side, qint16 level);

private slots:
    // funkcija koja zapravo vrsi pomeranja, poziva se kada istekne tajmer podesen u funkciji prepareNewRotation
    void rotateBox();

public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    CurrentBox currentBox() const;
    bool hasTheBall() const;
    void hasTheBall(bool hasTheBall);
    bool isRotating() const;
    bool isOpened() const;

    void openBox();
    void closeBox();

    // ovo su pocetne pozicije triju kutija, kadgod se izvrsi jedna rotacija u nizu rotacija sve tri kutije
    // moraju da budu na ove tri pozicije i da redom sadrze enume koji odgovaraju tim pozicijama
    constexpr static const QPointF boxPositions[3] = {
        QPointF(-400, -50), QPointF(-100, -50), QPointF(200, -50)
    };

    // slike koje ce se menjati u zavisnosti od toga da li je kutija otvorena ili zatvorena
    QImage *m_currentBoxPicture, m_closedBoxPicture, m_openedBoxPicture;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    // funkcija koja sluzi da zavrsi rotaciju ako smo dovoljno blizu krajnje pozicije
    bool posCloseTo(QPointF oldPos, QPointF newPos, QPointF endPos);

    QRectF m_boundingRect = QRectF(0, 0, 200, 200);
    CurrentBox m_currentBox;
    bool m_canOpenBox, m_boxIsOpened, m_hasTheBall;
    QTimer* m_timer;
    qint16 m_rotateSide;
    bool m_isRotating;
};

#endif // BOX_H
