#ifndef SIBICARENJEBACKGROUND_H
#define SIBICARENJEBACKGROUND_H

#include <QGraphicsObject>

class SibicarenjeBackground: public QGraphicsObject
{
    Q_OBJECT
public:
    SibicarenjeBackground();

    // QGraphicsItem interface
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    QImage m_image;
};

#endif // SIBICARENJEBACKGROUND_H
