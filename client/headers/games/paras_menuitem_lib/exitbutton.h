#ifndef EXITBUTTON_H
#define EXITBUTTON_H

#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

class ExitButton : public QGraphicsItem
{
public:
    ExitButton(qreal x, qreal y, qreal width, qreal height, const QString& text, const QColor& color = QColor("#b87a1d"));

    // QGraphicsItem interface
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    int type() const override;

private:
    QRectF m_rect;
    QString m_text;
    QColor m_color;
};

#endif // EXITBUTTON_H
