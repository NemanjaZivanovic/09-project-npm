#ifndef GAMEOVERPANEL_H
#define GAMEOVERPANEL_H

#include "menubutton.h"

#include <QGraphicsItem>
#include <QObject>
#include <QPainter>

#include <headers/games/game.h>

class GameOverPanel : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:

    enum { PlayAgainSP, NoPlayAgainSP, PlayAgainMP, NoPlayAgainMP };

    GameOverPanel(const QRectF rect, const int outcome, const bool multiplayer);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    MenuButton *yesBtn() const;
    MenuButton *noBtn() const;

private:
    QRectF m_rect;
    int m_outcome;
    bool m_multiplayer;
    MenuButton* m_yesBtn;
    MenuButton* m_noBtn;
};

#endif // GAMEOVERPANEL_H
