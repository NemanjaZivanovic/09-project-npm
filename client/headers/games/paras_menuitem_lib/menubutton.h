#ifndef MENUBUTTON_H
#define MENUBUTTON_H

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

class MenuButton : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    enum ButtonType { Custom, SinglePlayer, MultiPlayer, Easy, Medium, Hard, BackToMenu, ShowInstructions, CloseInstructions,
                        PlayAgainSP, NoPlayAgainSP, PlayAgainMP, NoPlayAgainMP };

    MenuButton(qreal x, qreal y, qreal width, qreal height, ButtonType type, const QString& text, const QColor& color = QColor("#b87a1d"));

    ButtonType btnType() const;

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

signals:
    void chooseMode(bool multiplayer);
    void backToSPMenu();
    void playAgainSP();
    void noPlayAgainSP();
    void playAgainMP();
    void noPlayAgainMP();
    void startSPGame(unsigned depth);
    void showInstructions();
    void closeInstructions();

private:
    QRectF m_rect;
    ButtonType m_btnType;
    QString m_text;
    QColor m_color;

    // QGraphicsItem interface
protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // MENUBUTTON_H
