#ifndef SMASH_ITBUTTON_H
#define SMASH_ITBUTTON_H

#include <QGraphicsObject>
#include <QObject>

class Smash_ItButton: public QGraphicsObject
{
    Q_OBJECT
public:
    Smash_ItButton(qint16 x, qint16 y, qint16 width, qint16 height, QString text);

signals:
    void endGame() const;
    void startGame();
    void restartGame();
    void singlePlayerMode();
    void multiPlayerMode();
    void returnToMenu();
    void instructions();

public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    int type() const override;
    void setText(QString&& text);

private:
    QRectF m_rect;
    QString m_text;
    qint16 m_x, m_y, m_width, m_height;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // SMASH_ITBUTTON_H
