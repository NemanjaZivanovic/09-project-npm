#ifndef GAME_H
#define GAME_H

#include "gameclient.h"
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QJsonObject>

#include <headers/dashboard/gameitem.h>

class Game : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Game)
public:
    // QGraphicsItem objekat koji implementira izlazak iz igre
    // na klik mora prevazidje f-ju type() i vrati Game::ExitGame
    enum { ExitGame = QGraphicsItem::UserType + 1 };

    // Koristi se za oznake igraca kao i za ishode igre
    enum Player { Invalid = 0, Player1 = 1, Player2 = 2, Draw = 3};

    Game(QGraphicsView* parent, GameItem::GameId gameId);

protected slots:
    virtual void error(QAbstractSocket::SocketError socketError) final;
    virtual void onConnectionError(QAbstractSocket::SocketError socketError) = 0;

    // Slot za akcije pri uspostavljanju veze sa serverom.
    // Ovde bi trebalo pozvati initNewGame metod.
    virtual void connectedToServer() = 0;
    // Slot za akcije pri prekidu konekcije sa serverom
    virtual void disconnectedFromServer() = 0;

    // Po spajanju dva igraca u igru, server salje igracu
    // da li je igrac 1 ili 2 i indeks igre (na serveru)
    virtual void gameInit(int player, int gameIndex);

    // Poziva se kada drugi igrac odigra potez
    virtual void opponentMove(const QJsonObject& move) = 0;

    // Po uspenom zavrsetku igre, server opet smesta igrace
    // igrace u red za cekanje. U potvrdnom, pozvati initNewGame(),
    // u suprotnom, prekinuti vezu pozivom disconnect().
    virtual void onNewGameOffer() = 0;

    // Ako je drugi igrac prekinuo vezu, server obavestava igraca o tome
    // i smesta ga opet u red za cekanje.
    virtual void onGameInterrupted() = 0;

protected:

    virtual void setGameScene() = 0;

    const GameClient* gameClient() const;

    Player player() const;
    void player(const Player &player);

    Player currentPlayer() const;
    void currentPlayer(const Player &currentPlayer);

    QGraphicsScene* scene() const;

    virtual void connectToServer() final;
    virtual void disconnectFromServer() final;

    // Trazi od server smestanje u red za igru
    virtual void initNewGame() final;

    // Salje igracev potez
    virtual void sendMove(QJsonObject& move, Player winner) final;

    QString errorMessage() const;

    GameItem::GameId m_gameId;
    Player m_player;
    Player m_currentPlayer;

private:
    QGraphicsView* m_gameParent;
    GameClient* m_gameClient;
    QString m_errorMessage;
    int m_gameIndex;
};

#endif // GAME_H
