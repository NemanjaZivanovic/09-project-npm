#ifndef ROBOT_H
#define ROBOT_H

#include <headers/games/game.h>
#include <headers/games/connect4/token.h>
#include <QThread>

class Robot : public QThread
{
    Q_OBJECT
public:
    Robot(const Game::Player& robotToken, const unsigned depth, const QVector<QVector<Token*>>& tokens, QObject* parent = nullptr);

    void getRobotMove();

    unsigned depth() const;

signals:
    void robotMadeMove(unsigned col);

    // QThread interface
protected:
    void run() override;

private:
    void boardToState(const QVector<QVector<Token*>>& tokens,
                      QVector<QVector<int>>& state, QVector<int>& top);
    int evaluate(const QVector<QVector<int>>& state, const QVector<int>& top);

    struct MinMaxNode {
      unsigned col;
      int value;
    };

    MinMaxNode minimize(QVector<QVector<int>>& state, QVector<int>& top,
                        int player, unsigned depth, int alpha, int beta);
    MinMaxNode maximize(QVector<QVector<int>>& state, QVector<int>& top,
                        int player, unsigned depth, int alpha, int beta);

private:
    Game::Player m_robotToken;
    unsigned m_depth;
    QVector<QVector<Token*>> m_tokens;
    unsigned m_humanTokens;
};

#endif // ROBOT_H
