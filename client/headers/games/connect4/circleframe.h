#ifndef CIRCLEFRAME_H
#define CIRCLEFRAME_H

#include <QGraphicsItem>
#include <QPainter>

class CircleFrame : public QGraphicsItem
{
public:
    CircleFrame(qreal r);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QRectF m_rect;
    QVector<QPointF> m_topRight;
    QVector<QPointF> m_topLeft;
    QVector<QPointF> m_bottomLeft;
    QVector<QPointF> m_bottomRight;
};

#endif // CIRCLEFRAME_H
