#ifndef TOKEN_H
#define TOKEN_H

#include <QGraphicsItem>

#include <headers/games/game.h>

class Token : public QGraphicsItem
{
public:
    Token(qreal width);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    Game::Player player() const;
    void player(const Game::Player &player);

    qreal dropStep() const;
    void dropStep(const qreal &dropStep);

    unsigned dropPhase() const;
    void dropPhase(unsigned dropPhase);

private:
    Game::Player m_player = Game::Invalid;
    QRectF m_rect;
    unsigned m_dropPhase;
    qreal m_dropStep;
};

#endif // TOKEN_H
