#ifndef BOARDCOLUMN_H
#define BOARDCOLUMN_H

#include <QGraphicsItem>
#include <QGraphicsSceneHoverEvent>
#include <QObject>
#include <QPainter>

class BoardColumn : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    BoardColumn(int index, qreal width, qreal height);

    int rowNum() const;
    int index() const;

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

signals:
    void columnHover(int index);
    void columnClick(int index);

    // QGraphicsItem interface
protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QRectF rect() const;

private:
    QRectF m_rect;
    int m_index;
    const int m_rowNum = 6;
};

#endif // BOARDCOLUMN_H
