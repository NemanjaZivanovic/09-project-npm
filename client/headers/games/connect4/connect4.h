#ifndef CONNECT4_H
#define CONNECT4_H

#include "gameboard.h"
#include "../paras_menuitem_lib/playerturninfo.h"
#include "robot.h"

#include <QGraphicsView>
#include <headers/games/game.h>
#include <QGraphicsScene>
#include <memory>

class Connect4 : public Game
{
    Q_OBJECT
public:
    Connect4(QGraphicsView* parent, GameItem::GameId gameId);
    ~Connect4();

    bool multiplayer() const;
    void multiplayer(bool multiplayer);

protected slots:
    void onConnectionError(QAbstractSocket::SocketError socketError) override;
    void connectedToServer() override;
    void disconnectedFromServer() override;
    void gameInit(int player, int gameIndex) override;
    void opponentMove(const QJsonObject &move) override;
    void onNewGameOffer() override;
    void onGameInterrupted() override;

    // Game interface
protected:
    void setGameScene() override;

private:
    void setMainMenu();
    void setConnectScene(const QString& message, const bool isError);
    void setGameOverPanel(const Player& winner);

private slots:
    void setSinglePlayerMenu();
    void onChooseMode(bool multiplayer);
    void onPickedMove(int index);
    void onColumnHover(int index);
    void onTokenDropped(int index);
    void onStartSPGame(unsigned depth);
    void onRobotMove(unsigned col);

    void playAgainSP();
    void noPlayAgainSP();
    void playAgainMP();
    void noPlayAgainMP();

private:
    bool m_multiplayer;
    GameBoard* m_gameBoard;
    Robot* m_robot;
    unsigned m_depth;
    PlayerTurnInfo* m_turnInfo;

};

#endif // CONNECT4_H
