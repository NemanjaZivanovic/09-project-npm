#ifndef TILE_H
#define TILE_H

#include "../game.h"
#include "viewbutton.h"
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QImage>
#include <QObject>
#include <QPainter>

class Tile : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    Tile(const qreal edgeLen, const int tileIndex);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    int battalions() const;
    int maxDefense() const;
    bool mobilizedTroops() const;
    void mobilizedTroops(bool mobilizedTroops);
    Game::Player owner() const;
    void owner(const Game::Player &owner);
    Game::Player whoseCapital() const;
    void setCapital(Game::Player player);
    bool hasGold() const;
    void hasGold(bool hasGold);
    int level() const;
    void level(int level);
    bool isLand() const;
    void isLand(bool isLand);
    int defense() const;
    void defense(int defense);
    void addNeighbor(int k);

    QVector<int>& neighbors();

    ViewButton::ViewType viewType() const;
    void setViewType(const ViewButton::ViewType &viewType);

signals:
    void tileSelected(int tileIndex);

    // QGraphicsItem interface
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QRectF m_rect;
    QVector<QPointF> m_points;
    int m_tileIndex;
    ViewButton::ViewType m_viewType;

    Game::Player m_owner;
    QVector<int> m_neighbors;
    bool m_isLand;
    Game::Player m_capital;
    bool m_hasGold;
    int m_level;
    bool m_mobilizedTroops;
    const int m_minDefense = 20;
    int m_defense;
};

#endif // TILE_H
