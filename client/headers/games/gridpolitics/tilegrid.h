#ifndef TILEGRID_H
#define TILEGRID_H

#include "tile.h"

#include <QGraphicsItem>
#include <QObject>
#include <QPainter>

class TileGrid : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    TileGrid();

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    unsigned sizeFactor() const;
    void initLand(const qreal landProbability, const qreal goldProbability);
    void setCapital(const int i, const Game::Player& player);
    Tile* getTile(const int i) const;
    int getGoldPerTurn(Game::Player player) const;
    int getMaxArmies(Game::Player player) const;
    bool isNeighbor(const int i, const Game::Player& player);
    void refreshMobilize();
    Game::Player checkState() const;
    void generateGrid();
    void setGeneralView();
    void setSpecificView(const Game::Player& player, const ViewButton::ViewType& view);

signals:
    void tileSelected(const int i);

public slots:
    void onTileSelected(const int i);

private:
    unsigned m_sizeFactor;
    QRectF m_rect;
    QVector<Tile*> m_tiles;
};

#endif // TILEGRID_H
