#ifndef GRAPH_H
#define GRAPH_H

#include "tile.h"

class Graph
{
public:
    void getNeighbors(QVector<Tile*>& tiles, const unsigned sizeFactor);

private:
    void getAdjacencyList(const QVector<Tile*>& tiles, const int n);
    bool inLeftCol(const int i, const int n);
    bool inRightCol(const int i, const int n);
    void getLandmasses();
    void dfsLandmass(const int i);
    void getPorts(const int landmass, QSet<int>& ports);
    void getEndPorts(QVector<Tile*>& tiles);
    void getLandmassEndPorts(const int landmass, const QSet<int>& startPorts, QVector<int>& endPorts);
    void addLandNeighbors(QVector<Tile*>& tiles);

private:
    QVector<QVector<int>> adjacencyList;
    QVector<QVector<int>> landmasses;
    QVector<int> landmassDist;
    QVector<bool> land;
    QVector<int> nodesLandmass;
    QVector<int> parents;
    QVector<std::pair<bool,int>> sailed;
};

#endif // GRAPH_H
