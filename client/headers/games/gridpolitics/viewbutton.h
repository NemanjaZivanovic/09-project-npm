#ifndef VIEWBUTTON_H
#define VIEWBUTTON_H

#include <headers/games/paras_menuitem_lib/menubutton.h>

class ViewButton : public MenuButton
{
    Q_OBJECT
public:
    enum ViewType { General, Level, Army, Defense };

    ViewButton(qreal x, qreal y, qreal width, qreal height, const QString& text, const ViewType& viewType, const QColor& color = QColor("#b87a1d"));

signals:
    void chooseView(const ViewType& view);
    // QGraphicsItem interface
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    ViewType m_type;
};

#endif // VIEWBUTTON_H
