QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/dashboard/gamedescriptionbox.cpp \
    src/games/gridpolitics/viewbutton.cpp \
    src/games/paras_menuitem_lib/instructionspanel.cpp \
    src/games/sibicarenje/notifications.cpp \
    src/games/connect4/boardcolumn.cpp \
    src/games/connect4/circleframe.cpp \
    src/games/connect4/connect4.cpp \
    src/games/paras_menuitem_lib/exitbutton.cpp \
    src/games/connect4/gameboard.cpp \
    src/games/paras_menuitem_lib/gameoverpanel.cpp \
    src/games/paras_menuitem_lib/menubutton.cpp \
    src/games/paras_menuitem_lib/messagefield.cpp \
    src/games/paras_menuitem_lib/playerturninfo.cpp \
    src/games/connect4/robot.cpp \
    src/games/connect4/token.cpp \
    src/games/game.cpp \
    src/dashboard/gameitem.cpp \
    src/dashboard/gamelist.cpp \
    src/dashboard/dashboard.cpp \
    src/games/gameclient.cpp \
    src/games/sibicarenje/box.cpp \
    src/games/sibicarenje/sibicarenje.cpp \
    src/games/sibicarenje/sibicarenjebackground.cpp \
    src/games/sibicarenje/ball.cpp \
    src/games/sibicarenje/sibicarenjebutton.cpp \
    src/games/gridpolitics/gamebar.cpp \
    src/games/gridpolitics/gridpolitics.cpp \
    src/games/gridpolitics/tile.cpp \
    src/games/gridpolitics/tilegrid.cpp \
    src/games/gridpolitics/tilepanel.cpp \
    src/games/gridpolitics/graph.cpp \
    src/games/gridpolitics/gridbot.cpp \
    src/games/sibicarenje/sibicarenjeinstructions.cpp \
    src/games/smash-it/field.cpp \
    src/games/smash-it/smash_it.cpp \
    src/games/smash-it/smash_itbutton.cpp \
    src/games/smash-it/smash_itinstructions.cpp \
    src/main.cpp \
    src/view.cpp \
    src/games/testgame/testgame.cpp \
    src/games/testgame/testexitrect.cpp

HEADERS += \
    headers/games/connect4/boardcolumn.h \
    headers/games/connect4/circleframe.h \
    headers/games/connect4/connect4.h \
    headers/dashboard/gamedescriptionbox.h \
    headers/games/gridpolitics/graph.h \
    headers/games/gridpolitics/viewbutton.h \
    headers/games/paras_menuitem_lib/exitbutton.h \
    headers/games/connect4/gameboard.h \
    headers/games/paras_menuitem_lib/gameoverpanel.h \
    headers/games/paras_menuitem_lib/instructionspanel.h \
    headers/games/paras_menuitem_lib/menubutton.h \
    headers/games/paras_menuitem_lib/messagefield.h \
    headers/games/paras_menuitem_lib/playerturninfo.h \
    headers/games/connect4/robot.h \
    headers/games/connect4/token.h \
    headers/games/game.h \
    headers/dashboard/gameitem.h \
    headers/dashboard/gamelist.h \
    headers/dashboard/dashboard.h \
    headers/games/gameclient.h \
    headers/games/sibicarenje/box.h \
    headers/games/sibicarenje/currentbox.h \
    headers/games/sibicarenje/sibicarenje.h \
    headers/games/sibicarenje/sibicarenjebackground.h \
    headers/games/sibicarenje/sibicarenjebutton.h \
    headers/games/gridpolitics/gamebar.h \
    headers/games/gridpolitics/gridpolitics.h \
    headers/games/gridpolitics/tile.h \
    headers/games/gridpolitics/tilegrid.h \
    headers/games/gridpolitics/tilepanel.h \
    headers/games/gridpolitics/graph.h \
    headers/games/gridpolitics/gridbot.h \
    headers/games/sibicarenje/sibicarenjeinstructions.h \
    headers/games/smash-it/field.h \
    headers/games/smash-it/smash_it.h \
    headers/games/smash-it/smash_itbutton.h \
    headers/games/smash-it/smash_itinstructions.h \
    headers/view.h \
    headers/games/testgame/testgame.h \
    headers/games/testgame/testexitrect.h \
    headers/games/sibicarenje/ball.h \
    headers/games/sibicarenje/notifications.h \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc

FORMS += \
    notifications.ui



