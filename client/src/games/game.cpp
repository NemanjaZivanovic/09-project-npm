#include <headers/games/game.h>

#include <QString>

Game::Game(QGraphicsView* parent, GameItem::GameId gameId)
    : QObject(static_cast<QObject*>(parent))
    , m_gameId(gameId)
    , m_player(Invalid)
    , m_currentPlayer(Player1)
    , m_gameParent(parent)
    , m_gameClient(new GameClient(this))
    , m_errorMessage(QString())
{
    scene()->clear();
    scene()->setBackgroundBrush(Qt::white);

    // sceneRect ce odgovarati prozoru a centar koordinatnog sistema u sredini
    scene()->setSceneRect(-scene()->width()/2, -scene()->height()/2, scene()->width(), scene()->height());
}

void Game::connectToServer()
{
    if(m_gameClient == nullptr) {
        m_gameClient = new GameClient(this);
    }

    m_gameClient->connectToServer();
}

void Game::disconnectFromServer()
{
    m_gameClient->disconnect();
}

/**
 * @brief Game::initNewGame
 *
 * Pozvati po uspesnoj konekciji sa serverom.
 * Trazi od servera smestanje u red za trenutnu igru.
 */
void Game::initNewGame()
{
    QJsonObject pickGameMsg;
    pickGameMsg[QObject::tr("type")] = QObject::tr("pickGame");
    pickGameMsg[QObject::tr("choice")] = static_cast<int>(m_gameId);

    m_gameClient->sendJson(pickGameMsg);
}

/**
 * @brief Game::sendMove
 * @param move Poruka u JSON formatu za razmenu poteza.
 * Ne sme da sadrzi polja 'type', 'outcome' ili 'gameIndex'.
 * (Rezervisana za kontrolne podatke na serveru)
 *
 * @param winner Status igre: Invalid - jos nema konacnog ishoda;
 * Player1 - pobedio 1. igrac; Player2 - pobedio 2. igrac; Draw - remi
 *
 * Salje odigran potez serveru.
 */

void Game::sendMove(QJsonObject& move, Player winner)
{
    move[tr("type")] = tr("play");
    move[tr("outcome")] = static_cast<int>(winner);
    move[tr("gameIndex")] = m_gameIndex;

    m_gameClient->sendJson(move);
}

/**
 * @brief Game::error
 * @param socketError
 *
 * Ucitava greske vezane za konekcije sa serverom.
 * Generise poruku greske koja ce biti dostupna podklasi.
 *
 * Pre toga mora da je pozove podklasa u svojoj implementaciji
 * slota onConnectionError na signal GameClient::error
 */
void Game::error(QAbstractSocket::SocketError socketError)
{
    QString errorMsg = nullptr;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::ProxyConnectionClosedError:
        // Ovim se bavi disconnectedFromServer()
        break;
    case QAbstractSocket::ConnectionRefusedError:
        errorMsg = QObject::tr("Error: The host refused the connection.");
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        errorMsg = QObject::tr("Error: The proxy refused the connection.");
        break;
    case QAbstractSocket::ProxyNotFoundError:
        errorMsg = QObject::tr("Error: Could not find the proxy.");
        break;
    case QAbstractSocket::HostNotFoundError:
        errorMsg = QObject::tr("Error: Could not find the server.");
        break;
    case QAbstractSocket::SocketAccessError:
        errorMsg = QObject::tr("Error: You don't have permissions to execute this operation.");
        break;
    case QAbstractSocket::SocketResourceError:
        errorMsg = QObject::tr("Error: Too many connections opened.");
        break;
    case QAbstractSocket::SocketTimeoutError:
        errorMsg = QObject::tr("Error: Operation timed out.");
        break;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        errorMsg = QObject::tr("Error: Proxy timed out.");
        break;
    case QAbstractSocket::NetworkError:
        errorMsg = QObject::tr("Error: Unable to reach the network.");
        break;
    case QAbstractSocket::UnknownSocketError:
        errorMsg = QObject::tr("Error: An unknown error occured.");
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        errorMsg = QObject::tr("Error: Operation not supported.");
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        errorMsg = QObject::tr("Error: Your proxy requires authentication.");
        break;
    case QAbstractSocket::ProxyProtocolError:
        errorMsg = QObject::tr("Error: Proxy comunication failed.");
        break;
    case QAbstractSocket::TemporaryError:
    case QAbstractSocket::OperationError:
        errorMsg = QObject::tr("Error: Operation failed, please try again.");
        break;
    default:
        Q_UNREACHABLE();
        break;
    }

    m_errorMessage = errorMsg;
}

void Game::gameInit(int player, int gameIndex)
{
    m_gameIndex = gameIndex;
    m_player = static_cast<Game::Player>(player);
}

QString Game::errorMessage() const
{
    return m_errorMessage;
}

Game::Player Game::currentPlayer() const
{
    return m_currentPlayer;
}

void Game::currentPlayer(const Player &currentPlayer)
{
    m_currentPlayer = currentPlayer;
}

QGraphicsScene *Game::scene() const
{
    return m_gameParent->scene();
}

Game::Player Game::player() const
{
    return m_player;
}

void Game::player(const Player &player)
{
    m_player = player;
}

const GameClient* Game::gameClient() const
{
    return m_gameClient;
}
