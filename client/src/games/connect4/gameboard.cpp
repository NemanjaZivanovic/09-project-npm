#include "../../../headers/games/connect4/gameboard.h"

#include <headers/games/connect4/boardcolumn.h>
#include <headers/games/connect4/circleframe.h>

#include <QBrush>

GameBoard::GameBoard(const qreal width)
{
    const qreal colWidth = width/(colNum() + 1);
    const qreal colGap = (width - colNum() * colWidth) / (colNum() + 1);
    const qreal colHeight = rowNum() * colWidth + (rowNum()-1) * colGap;

    const qreal boardHeight = colHeight + 2*colGap;
    m_rect = QRectF(-width/2, -boardHeight/2, width, boardHeight);

    // Rastojanje izmedju centara celija
    m_tokenStep = colWidth + colGap;

    // Inicijalne koordinate za zeton iznad table
    pendingStartX(0);
    pendingStartY(-colGap/2 - colWidth/2 - 3*tokenStep());

    // Crta se zeton iznad table
    m_pendingToken = new Token(colWidth + colGap);
    m_pendingToken->setParentItem(this);
    m_pendingToken->player(Game::Player1);
    m_pendingToken->setZValue(-1);
    m_pendingToken->setPos(pendingStartX(), pendingStartY());

    // Inicijalizuje se tabela za zetone.
    // Puni se cela zetonima samo da bi bili "tu", ali ce biti nevidljivi
    // sve dok im promenljiva m_player ima vrednost Game::Invalid

    m_tokens = QVector<QVector<Token*>>(rowNum());
    for(auto& tableRow : m_tokens) {
        tableRow = QVector<Token*>(colNum());
    }

    // Koordinate centra gonje leve celije table
    qreal cellX;
    qreal cellY = pendingStartY();

    // Postavljaju se "nevidljivi" zetoni na mesto
    for(int i=0; i<rowNum(); i++) {

        cellX = pendingStartX() - 3*tokenStep();
        cellY += tokenStep();
        for(int j=0; j<colNum(); j++) {

            // Token::m_player ima podrazumevanu Game::Invalid vrednost i ne crta se
            m_tokens[i][j] = new Token(colWidth);
            m_tokens[i][j]->setParentItem(this);
            m_tokens[i][j]->setPos(cellX, cellY);

            cellX += tokenStep();
        }
    }

    // Crtaju se kolone table
    for(int i=0; i<colNum(); i++) {
        BoardColumn* col = new BoardColumn(i, colWidth, colHeight);
        col->setParentItem(this);
        col->setPos((i-3)*colWidth + (i-3)*colGap, 0);

        connect(col, &BoardColumn::columnHover, this, &GameBoard::onColumnHover);
        connect(col, &BoardColumn::columnClick, this, &GameBoard::onColumnClick);
    }

    // Boje se razmaci izmedju celija table
    QPen pen = QPen(Qt::darkBlue);
    QBrush brush = QBrush(Qt::darkBlue);

    // Bojenje horizontalnih razmaka izmedju CircleFrame okvira / celija
    const qreal horizBarX = boundingRect().topLeft().x();
    qreal horizBarY = boundingRect().topLeft().y() + 0.4;

    while(horizBarY < boundingRect().bottomLeft().y()) {

        QGraphicsRectItem* horizBar =
                new QGraphicsRectItem(QRectF(horizBarX, horizBarY, boundingRect().width(), colGap - 0.8));
        horizBar->setPen(pen);
        horizBar->setBrush(brush);
        horizBar->setParentItem(this);

        horizBarY += colWidth + colGap;
    }

    // Bojenje vertikalnih razmaka
    qreal vertBarX = boundingRect().topLeft().x() + 0.4;
    const qreal vertBarY = boundingRect().topLeft().y();

    while(vertBarX < boundingRect().topRight().x()) {

        QGraphicsRectItem* vertBar =
                new QGraphicsRectItem(QRectF(vertBarX, vertBarY, colGap - 0.8, boundingRect().height()));
        vertBar->setPen(pen);
        vertBar->setBrush(brush);
        vertBar->setParentItem(this);

        vertBarX += colWidth + colGap;
    }
}

QRectF GameBoard::boundingRect() const
{
    return m_rect;
}

void GameBoard::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::darkBlue);
    painter->drawRect(m_rect);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

QRectF GameBoard::rect() const
{
    return m_rect;
}

int GameBoard::colNum() const
{
    return m_colNum;
}

qreal GameBoard::tokenStep() const
{
    return m_tokenStep;
}

qreal GameBoard::pendingStartX() const
{
    return m_pendingStartX;
}

void GameBoard::pendingStartX(const qreal &pendingX)
{
    m_pendingStartX = pendingX;
}

qreal GameBoard::pendingStartY() const
{
    return m_pendingStartY;
}

void GameBoard::pendingStartY(const qreal &pendingY)
{
    m_pendingStartY = pendingY;
}

/**
 * @brief GameBoard::dropToken
 * @param index Indeks kolone
 *
 * Pokrece animaciju pada zetona u kolonu
 */
void GameBoard::dropToken(int index)
{
    // Blokira se biranje novih poteza ili pomeranje zetona nad tablom
    droppingToken(true);
    pickedCol(index);

    // Postavljaju se pocetne i krajnje koordinate zetona
    m_pendingToken->setPos(pendingStartX() + (index-3)*tokenStep(), pendingStartY());

    int steps = 1;
    for(auto i = rowNum()-1; i >= 0; i--) {
        // Trazi se prvo slobodno mesto u koloni odozdo na gore
        if(m_tokens[i][pickedCol()]->player() == Game::Invalid) {
            // Mora +1 jer se padajuci zeton mora pomaci bar za jedan korak,
            // a red na vrhu ima indeks 0
            steps = i+1;
            break;
        }
    }

    droppingEndY(pendingStartY() + steps*tokenStep());

    m_dropTimer = new QTimer();
    m_pendingToken->dropPhase(1);
    m_pendingToken->dropStep(0.6);
    connect(m_dropTimer, SIGNAL(timeout()), this, SLOT(animateTokenDrop()));
    m_dropTimer->start(1000/60);
}

/**
 * @brief GameBoard::animateTokenDrop
 *
 * Vezuje se za tajmer dropTimer i animira
 * pad i odskakanje zetona.
 */
void GameBoard::animateTokenDrop()
{
    prepareGeometryChange();
    switch(m_pendingToken->dropPhase()) {

    // Prva faza - pocetno padanje kada se pusti zeton da pada
    // Treca faza - zeton opet pada ali se vise ne odbija, kraj animacije
    case 1:
    case 3: {
        m_pendingToken->setPos(m_pendingToken->x(),
                               m_pendingToken->y() + m_pendingToken->dropStep());

        // Proverava se da li je vreme za odskok (faza 1 -> faza 2)
        // ... ili kraj animacije (faza 3 -> stop)
        if(m_pendingToken->y() >= droppingEndY()) {

            m_pendingToken->setPos(m_pendingToken->x(), droppingEndY());

            if(m_pendingToken->dropPhase() == 1) {

                m_pendingToken->dropStep(-0.1 * m_pendingToken->dropStep());
                m_pendingToken->dropPhase(2);

            } else {

                m_dropTimer->stop();
                delete m_dropTimer;
                m_dropTimer = nullptr;

                // Posto je kraj animacije, rezervisani zeton u tabli dobija
                // boju po igracu koji je odigrao potez.
                // pendingToken se postavlja opet iznad table.

                for(int i=rowNum()-1; i>=0; i--) {
                    if(m_tokens[i][pickedCol()]->player() == Game::Invalid) {

                        // Trazi se prvo "slobodno" mesto u izabranoj koloni
                        // gde je upravo pao zeton i bojimo ga.
                        m_tokens[i][pickedCol()]->player(
                                    m_pendingToken->player());

                        // Postavlja se zeton za novi potez nad tablom i menja se boja
                        m_pendingToken->setPos(pendingStartX(), pendingStartY());
                        m_pendingToken->player( m_pendingToken->player() == Game::Player1 ?
                                                    Game::Player2 : Game::Player1);

                        emit droppedToken(pickedCol());

                        break;
                    }
                }

                droppingToken(false);
            }

        } else {
            // Brzina pada raste geometrijski
            m_pendingToken->dropStep(m_pendingToken->dropStep() * 1.15);
        }

        break;
    }

    // Druga faza - zeton se odbija
    case 2: {

        if(m_pendingToken->dropStep() >= -0.05) {

            // Ako je zeton bas usporio/stao sa odbitkom, krece faza 3
            m_pendingToken->dropStep(-1 * m_pendingToken->dropStep());
            m_pendingToken->dropPhase(3);

        } else {

            m_pendingToken->setPos(m_pendingToken->x(),
                                   m_pendingToken->y() + m_pendingToken->dropStep());

            // Zetonov odbitak brzo usporava
            m_pendingToken->dropStep(m_pendingToken->dropStep() * 0.6);
        }

        break;
    }

    default:
        break;
    }
}

/**
 * @brief GameBoard::onColumnHover
 * @param index Indeks kolone
 *
 * Slot koji omogucava pomeranje zetona nad tablom,
 * ali prvo pita Connect4 da li je dozvoljeno pomerati ga,
 * tj. da li je zeton korisnikov.
 */
void GameBoard::onColumnHover(int index)
{
    // Dok vec pada zeton ne moze se birati potez
    if(droppingToken()) {
        return;
    }

    emit checkValidHover(index);
}

/**
 * @brief GameBoard::onColumnClick
 * @param index Indeks kolone
 *
 * Slot koji reaguje na klik kolone, tj izbor poteza
 */
void GameBoard::onColumnClick(int index)
{
    // Dok vec pada zeton ne moze se birati potez
    if(droppingToken()) {
        return;
    }

    if(isValidMove(index)) {
        emit pickedMove(index);
    }
}

/**
 * @brief GameBoard::isValidMove
 * @param index Indeks kolone
 * @return true ako kolona nije popunjena do vrha, false inace
 */
bool GameBoard::isValidMove(int index)
{
    return m_tokens[0][index]->player() == Game::Invalid;
}

QVector<QVector<Token *> >& GameBoard::tokens()
{
    return m_tokens;
}

void GameBoard::pickedCol(int pickedCol)
{
    m_pickedCol = pickedCol;
}

/**
 * @brief GameBoard::moveHoverToken
 * @param index
 *
 * Poziva se samo ako je dozvoljeno pomerati
 * zeton, ako nije tudji.
 */
void GameBoard::moveHoverToken(int index)
{
    // X nad prvom kolonom + index*tokenStep
    const qreal translateX = pendingStartX() - 3*tokenStep() + index*tokenStep();

    m_pendingToken->setPos(translateX, pendingStartY());
}

int GameBoard::pickedCol() const
{
    return m_pickedCol;
}

qreal GameBoard::droppingEndY() const
{
    return m_droppingEndY;
}

void GameBoard::droppingEndY(const qreal &droppingEndY)
{
    m_droppingEndY = droppingEndY;
}

bool GameBoard::droppingToken() const
{
    return m_droppingToken;
}

void GameBoard::droppingToken(bool droppingToken)
{
    m_droppingToken = droppingToken;
}

int GameBoard::rowNum() const
{
    return m_rowNum;
}


/**
 *  Funkcija evaluira stanje za multiplayer.
 */
Game::Player GameBoard::checkState()
{
    /*
        Proverava se da li ima 4 u nizu po redovima.
        Ako u i-tom redu u koloni j=3 nema zetona, nema sigurno pobednika u tom redu,
        pa ni u gornjim redovima.
    */
    for(int i=rowNum()-1; i>=0 && m_tokens[i][3]->player() != Game::Invalid; i--) {
        for(int j=3; j<colNum(); j++) {
            if(m_tokens[i][j]->player() == m_tokens[i][j-1]->player()
                    && m_tokens[i][j-1]->player() == m_tokens[i][j-2]->player()
                    && m_tokens[i][j-2]->player() == m_tokens[i][j-3]->player()) {

                return m_tokens[i][j]->player();
            }
        }
    }
    /*
        Proverava se po kolonama.
        Ako na polju (i,j) nema zetona, tu nema 4 u nizu, sece se i pretraga za vise redove
    */
    for(int j=0; j<colNum(); j++) {
        for(int i=2; i>=0 && m_tokens[i][j]->player() != Game::Invalid; i--) {
            if(m_tokens[i][j]->player() == m_tokens[i+1][j]->player()
                    && m_tokens[i+1][j]->player() == m_tokens[i+2][j]->player()
                    && m_tokens[i+2][j]->player() == m_tokens[i+3][j]->player()) {

                return m_tokens[i][j]->player();
            }
        }
    }

    // Slicno za dijagonale oblika '/'
    for(int j=3; j<colNum(); j++) {
        for(int i=2; i>=0 && m_tokens[i][j]->player() != Game::Invalid; i--) {
            if(m_tokens[i][j]->player() == m_tokens[i+1][j-1]->player()
                    && m_tokens[i+1][j-1]->player() == m_tokens[i+2][j-2]->player()
                    && m_tokens[i+2][j-2]->player() == m_tokens[i+3][j-3]->player()) {

                return m_tokens[i][j]->player();
            }
        }
    }

    // Slicno za dijagonale oblika '\'
    for(int j=3; j>=0; j--) {
        for(int i=2; i>=0 && m_tokens[i][j]->player() != Game::Invalid; i--) {
            if(m_tokens[i][j]->player() == m_tokens[i+1][j+1]->player()
                    && m_tokens[i+1][j+1]->player() == m_tokens[i+2][j+2]->player()
                    && m_tokens[i+2][j+2]->player() == m_tokens[i+3][j+3]->player()) {

                return m_tokens[i][j]->player();
            }
        }
    }


    // Ako postoji bar jos jedno slobodno polje, igra se nastavlja
    for(auto& row : m_tokens) {
        if(std::any_of(std::begin(row), std::end(row), [](Token* t){ return t->player() == Game::Invalid; })) {

            return Game::Invalid;
        }
    }

    // Nema slobodnih polja, nereseno

    return Game::Draw;
}
