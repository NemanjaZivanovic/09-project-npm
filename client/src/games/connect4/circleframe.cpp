#include "../../../headers/games/connect4/circleframe.h"

#include <QBrush>
#include <QtMath>

CircleFrame::CircleFrame(qreal r)
    : m_rect(QRectF(-r/2, -r/2, r, r))
{
    // Imamo kvadrat r x r, kada se upise krug poluprecnika r,
    // zelimo da obojimo 4 preostala dela, oni zajedno cine 'CircleFrame'

    // To su 4 konveksna poligona, inicijalizujemo te tacke i cuvamo u posebnim
    // vektorima, koje ce koristiti paint() za crtanje 4 QPolygonF-a.

    m_bottomRight.push_back(QPointF(r, r));

    const qreal angleStep = M_PI/100;
    for(qreal angle = 0; angle <= M_PI/2; angle += angleStep) {
        m_bottomRight.push_back(QPointF(r * qCos(angle), r * qSin(angle)));
    }

    m_bottomLeft.push_back(QPointF(-r, r));
    for(qreal angle = M_PI/2; angle <= M_PI; angle += angleStep) {
        m_bottomLeft.push_back(QPointF(r * qCos(angle), r * qSin(angle)));
    }

    m_topLeft.push_back(QPointF(-r, -r));
    for(qreal angle = M_PI; angle <= 3*M_PI/2; angle += angleStep) {
        m_topLeft.push_back(QPointF(r * qCos(angle), r * qSin(angle)));
    }

    m_topRight.push_back(QPointF(r, -r));
    for(qreal angle = 3*M_PI/2; angle <= 2*M_PI; angle += angleStep) {
        m_topRight.push_back(QPointF(r * qCos(angle), r * qSin(angle)));
    }
}

QRectF CircleFrame::boundingRect() const
{
    return m_rect;
}

void CircleFrame::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::darkBlue);
    painter->setBrush(QBrush(Qt::darkBlue));

    painter->drawConvexPolygon(m_topRight);
    painter->drawConvexPolygon(m_topLeft);
    painter->drawConvexPolygon(m_bottomLeft);
    painter->drawConvexPolygon(m_bottomRight);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}
