#include "../../../headers/games/gridpolitics/tile.h"

#include <QtMath>
#include <QDebug>

Tile::Tile(const qreal edgeLen, const int tileIndex)
    : m_rect(QRectF(-edgeLen, -edgeLen, 2*edgeLen, 2*edgeLen))
    , m_tileIndex(tileIndex)
    , m_viewType(ViewButton::General)
    , m_owner(Game::Invalid)
    , m_isLand(false)
    , m_capital(Game::Invalid)
    , m_hasGold(false)
    , m_level(8)
    , m_mobilizedTroops(false)
    , m_defense(20)
{
    // Dodaju se tacke pravilnog sestougla
    for(qreal angle = 0; angle < 2*M_PI; angle += M_PI/3) {
        m_points.push_back(QPointF(edgeLen*qCos(angle), edgeLen*qSin(angle)));
    }

    setFlag(QGraphicsItem::ItemIsSelectable);
}

QRectF Tile::boundingRect() const
{
    return m_rect;
}

void Tile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen("#cccccc");

    // Odredjivanje boje pozadine
    if(isLand()) {
        switch(owner()) {
        case Game::Player1: {
            if(isSelected()) {
                painter->setBrush(QBrush("#ff8e7d"));
            } else {
                painter->setBrush(QBrush("#f55a42"));
            }
            break;
        }
        case Game::Player2: {
            if(isSelected()) {
                painter->setBrush(QBrush("#6c87f5"));
            } else {
                painter->setBrush(QBrush("#1e40c9"));
            }
            break;
        }
        default: {
            // Kopno koje ne pripada nikom
            if(isSelected()) {
                painter->setBrush(QBrush("#6dbf69"));
            } else {
                painter->setBrush(QBrush("#588b46"));
            }
            break;
        }
        }

    } else {
        // Voda
        painter->setBrush(QBrush("#51c9d2"));
    }

    painter->drawConvexPolygon(m_points);

    switch(viewType()) {
    case ViewButton::General: {
        // Ukoliko je opsti pogled prikazuju se zlatnici ili simboli glavnih gradova
        // (ako cvor ima neki od ta dva statusa)

        if(whoseCapital() != Game::Invalid && whoseCapital() == owner()) {
            // Za glavni grad se crta zuti simbol jednog vojnickog cina
            const qreal halfRadius = (m_rect.right() - m_rect.center().x())/2;
            QPointF tip = m_rect.center();
            tip.setY(tip.y() - halfRadius);

            QPointF lowLeft = m_rect.center();
            lowLeft.setX(lowLeft.x() - halfRadius);
            lowLeft.setY(lowLeft.y() + halfRadius/2);

            QPointF lowRight = m_rect.center();
            lowRight.setX(lowRight.x() + halfRadius);
            lowRight.setY((lowRight.y() + halfRadius/2));

            painter->setPen(QPen(Qt::yellow, 2));
            painter->drawLine(tip, lowLeft);
            painter->drawLine(tip, lowRight);
        }

        if(hasGold()) {
            // Rub zlatnika
            painter->setPen("#d3c203");
            painter->setBrush(QBrush("#d3c203"));
            painter->drawEllipse(m_rect.center(), 6, 7);

            // Unutrasnji, tamniji deo zlatnika
            painter->setPen("#c4b403");
            painter->setBrush(QBrush("#c4b403"));
            painter->drawEllipse(m_rect.center(), 4, 5);
        }
        break;
    }
    case ViewButton::Level: {
        // Prikazuje se level cvora
        painter->drawText(m_rect, Qt::AlignCenter, QString::number(level()));

        break;
    }
    case ViewButton::Army: {
        // Prikazuje se level cvora
        painter->drawText(m_rect, Qt::AlignCenter, QString::number(battalions()));

        break;
    }
    case ViewButton::Defense: {
        // Prikazuje se level cvora
        painter->drawText(m_rect, Qt::AlignCenter, QString::number(defense()));

        break;
    }
    default:
        break;
    }

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

int Tile::battalions() const
{
    return 4 + m_level/2;
}

int Tile::maxDefense() const
{
    return 25 + 5*m_level;
}

bool Tile::mobilizedTroops() const
{
    return m_mobilizedTroops;
}

void Tile::mobilizedTroops(bool mobilizedTroops)
{
    m_mobilizedTroops = mobilizedTroops;
}

Game::Player Tile::owner() const
{
    return m_owner;
}

void Tile::owner(const Game::Player &owner)
{
    m_owner = owner;
}

Game::Player Tile::whoseCapital() const {
    return m_capital;
}

void Tile::setCapital(Game::Player player) {
    m_capital = player;
}

bool Tile::hasGold() const
{
    return m_hasGold;
}

int Tile::level() const
{
    return m_level;
}

bool Tile::isLand() const
{
    return m_isLand;
}

int Tile::defense() const
{
    return m_defense;
}

void Tile::defense(int defense)
{
    m_defense = defense;
}

/**
 * @brief Tile::addNeighbor
 * @param k Indeks suseda
 *
 * Dodaje indeks suseda u listu.
 */
void Tile::addNeighbor(int k)
{
    if(!m_neighbors.contains(k)) {
        m_neighbors.push_back(k);
    }
}

void Tile::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    event->accept();
}

void Tile::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    if(event->button() == Qt::MouseButton::LeftButton) {
        setSelected(true);
        emit tileSelected(m_tileIndex);
    }
}

ViewButton::ViewType Tile::viewType() const
{
    return m_viewType;
}

void Tile::setViewType(const ViewButton::ViewType &viewType)
{
    m_viewType = viewType;
}

QVector<int>& Tile::neighbors()
{
    return m_neighbors;
}

void Tile::isLand(bool isLand)
{
    m_isLand = isLand;
}

void Tile::level(int level)
{
    m_level = level;
}

void Tile::hasGold(bool hasGold)
{
    m_hasGold = hasGold;
}
