#include "../../../headers/games/gridpolitics/tilepanel.h"

TilePanel::TilePanel(const qreal x, const qreal y, const qreal width, const qreal height,
                     const int level, const int battalions, const int defense)
    : m_rect(QRectF(x, y, width, height))
    , m_level(level)
    , m_battalions(battalions)
    , m_mobilizedTroops(false)
    , m_defense(defense)
{
}

QRectF TilePanel::boundingRect() const
{
    return m_rect;
}

void TilePanel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // Crtanje okvira i bojenje pozadine
    painter->setBrush(QBrush("#3c81aa"));
    painter->drawRect(m_rect);

    QFont font;
    font.setPixelSize(13);
    painter->setFont(font);

    painter->drawText(m_rect.topLeft().x(), m_rect.topLeft().y(), m_rect.width(), m_rect.height()/5,
                      Qt::AlignCenter, tr("Territory details"));

    font.setPixelSize(15);
    painter->setFont(font);

    painter->drawText(m_rect.topLeft().x() + m_rect.width()/8, m_rect.topLeft().y() + m_rect.height()/4,
                      m_rect.width(), m_rect.height()/5, Qt::AlignTop,
                      tr("Level: ") + QString::number(m_level));

    painter->drawText(m_rect.topLeft().x() + m_rect.width()/8, m_rect.topLeft().y() + 2*m_rect.height()/4,
                      m_rect.width(), m_rect.height()/5, Qt::AlignTop,
                      tr("Battalions: ") + (m_mobilizedTroops ? "0/" : QString::number(m_battalions) + "/") + QString::number(m_battalions));

    painter->drawText(m_rect.topLeft().x() + m_rect.width()/8, m_rect.topLeft().y() + 3*m_rect.height()/4,
                      m_rect.width(), m_rect.height()/5, Qt::AlignTop,
                      tr("Defense: ") + QString::number(m_defense));

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

void TilePanel::setLevel(int level)
{
    m_level = level;
}

void TilePanel::setBattalions(int battalions)
{
    m_battalions = battalions;
}

void TilePanel::setMobilizedTroops(bool mobilizedTroops)
{
    m_mobilizedTroops = mobilizedTroops;
}

void TilePanel::setDefense(int defense)
{
    m_defense = defense;
}

int TilePanel::level() const
{
    return m_level;
}

int TilePanel::battalions() const
{
    return m_battalions;
}

int TilePanel::defense() const
{
    return m_defense;
}

