#include "../../../headers/games/gridpolitics/graph.h"
#include <algorithm>

/**
 * @brief Graph::getNeighbors
 * @param tiles
 * @param sizeFactor
 *
 * Jedini interfejs sa klasom TileGrid. Generise listu validnih
 * suseda i popunjava odgovarajuce liste unutar prosledjenog niza tile-ova.
 */
void Graph::getNeighbors(QVector<Tile *> &tiles, const unsigned sizeFactor)
{
    land = QVector<bool>(tiles.size(), false);
    nodesLandmass = QVector<int>(tiles.size(), -1);
    parents = QVector<int>(tiles.size(), -1);
    sailed = QVector<std::pair<bool,int>>(tiles.size(), std::make_pair<bool,int>(false, -1));

    getAdjacencyList(tiles, sizeFactor);
    getLandmasses();
    getEndPorts(tiles);
    addLandNeighbors(tiles);
}

/**
 * @brief Graph::getAdjacencyList
 * @param tiles Niz Tile-ova
 * @param n Size factor
 *
 * Generise pocetnu matricu suseda potrebnu za filtriranje
 * validnih suseda.
 */
void Graph::getAdjacencyList(const QVector<Tile*>& tiles, const int n)
{
    // Indeksna aritmetika za nalazenje neposrednih suseda
    QVector<int> tileSteps;
    tileSteps.push_back(-2*n + 1);     // North
    tileSteps.push_back(-n + 1);       // NorthEast
    tileSteps.push_back(n);            // SouthEast
    tileSteps.push_back(2*n - 1);      // South
    tileSteps.push_back(n - 1);        // SouthWest
    tileSteps.push_back(-n);           // NorthWest

    for(auto i=0; i<tiles.size(); i++) {
        adjacencyList.push_back(QVector<int>());

        land[i] = tiles[i]->isLand();

        for(auto step : tileSteps) {
            const int neighbor = i + step;
            if(neighbor < 0 || neighbor >= tiles.size()) {
                // Predjene granice grida
                continue;
            }

            // Ako je i-ti cvor u krajnjoj levoj koloni,
            // ne moze imati NW i SW susede (neighbor bi tada bio u desnoj)
            if(inLeftCol(i, n) && inRightCol(neighbor, n)) {
                continue;
            }

            // Ako je i-ti cvor u krajnjoj desnoj koloni,
            // ne moze imati NE i SE susede (neighbor bi tada bio u levoj)
            if(inRightCol(i, n) && inLeftCol(neighbor, n)) {
                continue;
            }

            adjacencyList[i].push_back(neighbor);
        }
    }
}

/**
 * @brief Graph::inLeftCol
 * @param i Indeks cvora
 * @param n Size factor
 * @return Vraca true ako je cvor u levoj koloni grida, inace false.
 */
bool Graph::inLeftCol(const int i, const int n)
{
    const int bottomLeft = (n-1)*n + (n-1)*(n-1);
    for(auto j=0; j<=bottomLeft; j += 2*n-1) {
        if(i == j) {
            return true;
        }
    }

    return false;
}

/**
 * @brief Graph::inRightCol
 * @param i Indeks cvora
 * @param n Size factor
 * @return Vraca true ako je cvor u levoj koloni grida, inace false.
 */
bool Graph::inRightCol(const int i, const int n)
{
    const int bottomRight = n*n + (n-1)*(n-1) - 1;
    for(auto j=n-1; j<=bottomRight; j += 2*n-1) {
        if(i == j) {
            return true;
        }
    }

    return false;
}

/**
 * @brief Graph::getLandmasses
 *
 * Svakom kopnenom cvoru se odredjuje kojoj kopnenoj celini
 * pripada. Za svaku kopnenu celinu se cuvaju cvorovi koje sadrzi.
 */
void Graph::getLandmasses()
{
    for(auto i=0; i<adjacencyList.size(); i++) {
        if(land[i] && nodesLandmass[i] == -1) {
            // Cvor i je kopno ali jos nije utvrdjen id kopnene celine
            // tj komponente povezanosti (ako se racuna samo kopno)

            // U ovu granu se ulazi svaki put kada se "otkrije"
            // novi landmass, pa se dodaje u vektor
            landmasses.push_back(QVector<int>());

            // Pocinje dfs pretraga
            dfsLandmass(i);
        }
    }

    // Pronadjen je odredjen broj kopnenih celina
    // pa se inicijalizuje donji niz te duzine.
    // Potrebno za trazenje prekomorskih puteva.
    landmassDist = QVector<int>(landmasses.size());
}

/**
 * @brief Graph::dfsLandmass
 * @param i Indeks cvora
 *
 * Verzija DFS algoritma koji nalazi sve cvorove u jednoj
 * kopnenoj celini.
 */
void Graph::dfsLandmass(const int i)
{
    // Cvoru se dodaje id kopnene celine
    nodesLandmass[i] = landmasses.size()-1;
    // Kopnenoj celini se dodaje cvor
    landmasses.back().push_back(i);

    for(auto neighbor : adjacencyList[i]) {
        if(land[neighbor] && nodesLandmass[neighbor] == -1) {
            // Pretrazuju se samo susedni kopneni cvorovi
            // nodeLandmass[neighbor] == -1 je pandan za visited == true
            dfsLandmass(neighbor);
        }
    }
}

/**
 * @brief Graph::getPorts
 * @param landmass Indeks kopnene celine
 * @param ports Prazan skup koji se puni okolnim 'morskim' cvorovima
 *
 * Za dati landmass i prazan skup, funkcija u isti skup ubacuje one
 * 'morske' cvorove koji neposredno okruzuju taj landmass.
 */
void Graph::getPorts(const int landmass, QSet<int> &ports)
{
    for(auto node : landmasses[landmass]) {
        for(auto neighbor : adjacencyList[node]) {
            if(!land[neighbor]) {
                ports.insert(neighbor);
            }
        }
    }
}

/**
 * @brief Graph::getEndPorts
 * @param tiles TileGrid mreza cvorova
 *
 * Cvorovima iz TileGrid dodaje prekomorske susedne cvorove.
 */
void Graph::getEndPorts(QVector<Tile *> &tiles)
{
    for(auto i=0; i<landmasses.size(); i++) {
        // Traze se "luke" za trenutni landmass
        QSet<int> startPorts;
        getPorts(i, startPorts);

        // Traze se prekomorske luke za trenutni landmass
        QVector<int> endPorts;
        getLandmassEndPorts(i, startPorts, endPorts);

        for(auto endPort : endPorts) {
            // Trazi se od koje luke je krenula pretraga do cilja
            int parent = parents[endPort];
            int dist = 0;
            while(parents[parent] != -1) {
                parent = parents[parent];
                dist++;
            }

            // "Luka" je morski cvor koji okruzuje zadati landmass
            // Dakle od njegovih kopnenih suseda koji pripadaju
            // zadatoj landmass se moze stici do cvora endPort

            for(auto node : adjacencyList[parent]) {

                if(land[node] && nodesLandmass[node] == i && dist+1 == landmassDist[i]) {

                    tiles[node]->addNeighbor(endPort);
                }

            }
        }
    }
}

/**
 * @brief Graph::getLandmassEndPorts
 * @param landmass Indeks kopnene celine
 * @param startPorts Skup morskih cvorova koji je okruzuju
 * @param endPorts Niz najblizih kopnenih cvorova do kojih
 * se je doslo morskim putem od date landmass
 */
void Graph::getLandmassEndPorts(const int landmass, const QSet<int> &startPorts, QVector<int>& endPorts)
{
    // Na pocetku se mora resetovati lista roditelja, sailed niz,
    // i indikatori da li je pronadjen drugi landmass
    parents.fill(-1, parents.size());
    sailed.fill(std::make_pair<int,int>(false, -1));
    // Cuva najkrace putanje do kopnenih celina
    landmassDist.fill(std::numeric_limits<int>::max());

    QVector<int> nodeQueue;
    for(auto port : startPorts) {
        nodeQueue.push_back(port);
        // Oznacavamo ih kao posecene ali se putanja azurira tek kada dodju na red
        sailed[port].first = true;
    }

    while(!nodeQueue.empty()) {
        int node = nodeQueue.front();
        nodeQueue.pop_front();

        // Doplovljeno je do ovog cvora i povecava se duzina predjenog puta
        sailed[node].first = true;
        sailed[node].second++;

        for(auto nextNode : adjacencyList[node]) {
            if(nodesLandmass[nextNode] == landmass
                    || sailed[nextNode].first) {
                // Preskace se cvor trenutne kopnene celije od koje se traze druge
                // Ili ako se je vec doplovilo do nje
                continue;
            }

            // Postavlja se roditelj na trenutni cvor
            parents[nextNode] = node;

            // Razmatraju se kraci putevi do cvora
            if(land[nextNode] && (sailed[node].second + 1 <= landmassDist[landmass])) {

                landmassDist[landmass] = sailed[node].second + 1;

                sailed[nextNode].first = true;
                // Postavlja se duzina puta
                sailed[nextNode].second = sailed[node].second + 1;
                // Dodaje se u rezultat (ne i u red nodeQueue)
                endPorts.push_back(nextNode);

            } else if(!land[nextNode]) {
                // Ako nije kopno, dodaju se validni "morski" cvorovi u red
                // tj oni do kojih se nije doplovilo
                for(auto seaNext : adjacencyList[nextNode]) {
                    if(!sailed[seaNext].first) {
                        nodeQueue.push_back(seaNext);
                    }
                }
            }
        }
    }
}

/**
 * @brief Graph::addLandNeighbors
 * @param tiles Niz Tile-ova iz grida.
 *
 * Kopnenim cvorovima se dodaju njihovi kopneni susedi.
 */
void Graph::addLandNeighbors(QVector<Tile *> &tiles)
{
    for(auto i=0; i<adjacencyList.size(); i++) {
        if(!land[i]) {
            // Samo kopnenim cvorovima se dodaju susedi
            continue;
        }

        for(auto neighbor : adjacencyList[i]) {
            if(land[neighbor]) {
                tiles[i]->addNeighbor(neighbor);
            }
        }
    }
}
