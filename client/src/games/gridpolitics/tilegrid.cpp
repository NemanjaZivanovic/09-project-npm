#include "../../../headers/games/gridpolitics/tilegrid.h"

#include <QtMath>
#include <numeric>
#include <random>

#include <headers/games/gridpolitics/graph.h>

TileGrid::TileGrid()
    : m_sizeFactor(9)
{
    // Stranica sestougla
    const qreal tileEdge = 25;

    // Odredjuju se dimenzije boundingRect sa centrom u sredini prozora
    const qreal width = sizeFactor()*(2*tileEdge) + (sizeFactor()-1)*tileEdge;

    // Visina jednakostranicnog trougla u pravilnom sestouglu
    const qreal trigHeight = tileEdge * qSqrt(3) / 2;
    const qreal height = sizeFactor()*(2*trigHeight);
    m_rect = QRectF(-width/2, -height/2, width, height);


    // Centar gornjeg levog sestougla
    qreal x = m_rect.left() + tileEdge;
    qreal y = m_rect.top() + trigHeight;

    // Dodavanje cvorova na scenu
    for(auto i=0u; i<sizeFactor(); i++) {
        // Petlja za dodavanje cvorova u "siri" red matrice
        for(auto j = 0u; j<sizeFactor(); j++) {
            m_tiles.push_back(new Tile(tileEdge, m_tiles.size()));

            m_tiles.back()->setPos(x, y);
            m_tiles.back()->setParentItem(this);
            connect(m_tiles.back(), &Tile::tileSelected, this, &TileGrid::tileSelected);

            x += 3*tileEdge;
        }

        if(i == sizeFactor()-1) {
            break;
        }

        x = m_rect.left() + 2*tileEdge + tileEdge/2;
        y += trigHeight;
        // Petlja za dodavanje cvorova u "uzi" red matrice
        for(auto j = 0u; j<sizeFactor()-1; j++) {
            m_tiles.push_back(new Tile(tileEdge, m_tiles.size()));

            m_tiles.back()->setPos(x, y);
            m_tiles.back()->setParentItem(this);
            connect(m_tiles.back(), &Tile::tileSelected, this, &TileGrid::tileSelected);

            x += 3*tileEdge;
        }

        x = m_rect.left() + tileEdge;
        y += trigHeight;
    }

    // Generise se kopno, zlatnici i susedi cvorova
    const qreal landProb = 0.5; // verovatnoca da je cvor kopno
    const qreal goldProb = 0.2; // verovatnoca da cvor ima zlatnik ako je kopno
    initLand(landProb, goldProb);
}

QRectF TileGrid::boundingRect() const
{
    return m_rect;
}

void TileGrid::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(option);
    Q_UNUSED(widget)
}

unsigned TileGrid::sizeFactor() const
{
    return m_sizeFactor;
}

void TileGrid::initLand(const qreal landProbability, const qreal goldProbability)
{
    if(landProbability <= 0 || 1 <= landProbability
            || goldProbability <= 0 || 1 <= goldProbability) {
        // Date erovatnoce mora biti u domenu (0,1)
        return;
    }

    // Fiksirane pocetne teritorije za prvog i drugog igraca

    // Player1
    auto player1 = std::make_pair<int,int>(95, 104);
    m_tiles[player1.first]->isLand(true);
    m_tiles[player1.first]->owner(Game::Player1);
    m_tiles[player1.first]->setCapital(Game::Player1);
    m_tiles[player1.second]->isLand(true);
    m_tiles[player1.second]->owner(Game::Player1);

    // Player1
    auto player2 = std::make_pair<int,int>(49, 40);
    m_tiles[player2.first]->isLand(true);
    m_tiles[player2.first]->owner(Game::Player2);
    m_tiles[player2.first]->setCapital(Game::Player2);
    m_tiles[player2.second]->isLand(true);
    m_tiles[player2.second]->owner(Game::Player2);

    // Promenljive potrebne za nasumicno odredjivanje
    // da li je cvor kopno, i ako da, da li sadrzi zlatnik
    std::random_device rd;
    std::mt19937 gen(rd());

    // Koristi se Bernulijeva raspodela za date verovatnoce
    std::bernoulli_distribution land(landProbability);
    std::bernoulli_distribution gold(goldProbability);

    for(auto i=0; i<m_tiles.size(); i++) {
        // Preskacu se definisani cvorovi jer se vec zna da su kopno i da ne sadrze zlatnike
        if(i == player1.first || i == player1.second
                || i == player2.first || i == player2.second) {
            continue;
        }

        const bool isLand = land(gen);
        m_tiles[i]->isLand(isLand);
        if(isLand) {
            m_tiles[i]->hasGold(gold(gen));
        }
    }
}

Tile *TileGrid::getTile(const int i) const
{
    return m_tiles[i];
}

/**
 * @brief TileGrid::getGoldPerTurn
 * @param player Igrac za kojeg se trazi broj zlatnika
 * @return Ukupan broj zlatnika koje igrac dobija na pocetku
 * sledeceg poteza
 */
int TileGrid::getGoldPerTurn(Game::Player player) const
{
    // Igrac dobija zlatnik za one celije koje ima u posedu
    // i koje imaju zlatnik ili su njihov glavni grad
    int gold = std::accumulate(std::cbegin(m_tiles), std::cend(m_tiles), 0,
        [player](int acc, const Tile* tile)
        {
            if(tile->owner() == player
                    && (tile->whoseCapital() == player
                        || tile->hasGold())) {
                ++acc;
            }
            return acc;
        }
    );

    return gold;
}

int TileGrid::getMaxArmies(Game::Player player) const
{
    int levelSum = std::accumulate(std::cbegin(m_tiles), std::cend(m_tiles), 0,
        [player](int acc, const Tile* tile)
        {
            if(tile->owner() == player) {
                    acc += tile->level();
            }
            return acc;
        }
    );

    return std::max(levelSum, 50);
}

/**
 * @brief TileGrid::isNeighbor
 * @param i Indeks cvora za koji se gleda da li je susedu player-u
 * @param player Vlasnik cvorova za koje se gleda da li su susedi za 'i'
 * @return
 *
 * Vraca true ako je cvor 'i' sused nekom od cvorova u posedu player-a
 */
bool TileGrid::isNeighbor(const int i, const Game::Player &player)
{
    const auto tiles = m_tiles;
    return std::cend(m_tiles[i]->neighbors())
            != std::find_if(std::cbegin(m_tiles[i]->neighbors()), std::cend(m_tiles[i]->neighbors()),
                [tiles, player](auto index)
                {
                    return tiles[index]->owner() == player;
                }
    );
}

/**
 * @brief TileGrid::refreshMobilize
 *
 * Omogucava se mobilizacija na svim cvorovima
 * kada je korisnik opet na potezu
 */
void TileGrid::refreshMobilize()
{
    for(auto tile : m_tiles) {
        tile->mobilizedTroops(false);
    }
}

/**
 * @brief TileGrid::checkState
 * @return Igrac koji je pobedio ili Game::Invalid
 *
 * Proveravaju se uslovi za kraj igre.
 * Igrac je izgubio ako vise nema u posedu svoj glavni cvor
 * i nijednu teritoriju sa zlatnikom.
 */
Game::Player TileGrid::checkState() const
{
    // Potrebno je samo proveriti da li ce igrac dobiti
    // neki zlatnik pri sledecem potezu ili je izgubio
    // sve cvorove za tako nesto
    if(getGoldPerTurn(Game::Player1) == 0) {
        return Game::Player2;
    }

    // Mora se proveriti i za drugog igraca
    if(getGoldPerTurn(Game::Player2) == 0) {
        return Game::Player1;
    }

    return Game::Invalid;
}

/**
 * @brief TileGrid::generateGrid
 *
 * Generise random mapu.
 */
void TileGrid::generateGrid()
{
    Graph g;
    g.getNeighbors(m_tiles, sizeFactor());
}

/**
 * @brief TileGrid::setGeneralView
 *
 * Svaki cvor ce imati general view.
 */
void TileGrid::setGeneralView()
{
    for(auto tile : m_tiles) {
        tile->setViewType(ViewButton::General);
    }
}

/**
 * @brief TileGrid::setSpecificView
 * @param player Korisnik
 * @param view Zeljeni tip pogleda
 *
 * Za svaki cvor koji pripada korisniku i njegovim direktnim
 * susedima se prikazuje odgovarajuca vrednost level, battalions ili defense
 */
void TileGrid::setSpecificView(const Game::Player& player, const ViewButton::ViewType& view)
{
    for(auto tile : m_tiles) {
        if(tile->owner() == player) {
            tile->setViewType(view);
            // Isto za sve njegove susede
            for(auto neighbor : tile->neighbors()) {
                m_tiles[neighbor]->setViewType(view);
            }
        }
    }
}

/**
* @brief TileGrid::tileSelected
* @param i Indeks celije
*
* Kada se celija klikne, salju se podaci o njoj glavnoj
* klasi za prikaz u TilePanel.
*/
void TileGrid::onTileSelected(const int i)
{
    emit tileSelected(i);
}


