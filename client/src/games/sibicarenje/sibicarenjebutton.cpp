#include "headers/games/sibicarenje/sibicarenjebutton.h"
#include "headers/games/game.h"

#include <QGraphicsSceneMouseEvent>
#include <QPainter>


SibicarenjeButton::SibicarenjeButton(qint16 x, qint16 y, qint16 width, qint16 height, QString text):
    m_text(text), m_x(x), m_y(y), m_width(width), m_height(height)
{
    m_boundingRect = QRectF(m_x, m_y, m_width, m_height);
}

QRectF SibicarenjeButton::boundingRect() const
{
    return m_boundingRect;
}

void SibicarenjeButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor color;
    color.setRgb(239,239,239);
    painter->setBrush(color);

    painter->drawEllipse(boundingRect());
    painter->drawText(boundingRect(), Qt::AlignCenter, m_text);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void SibicarenjeButton::setText(QString text)
{
    m_text = text;
}

int SibicarenjeButton::type() const
{
    scene()->clear();
    return Game::ExitGame;
}

void SibicarenjeButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    auto text = m_text;

    emit clicked();

    if(text == "exit"){
        // ne zelimo da prihvatamo dogadjaj, tj zelimo da view odradi svoje
        // i izbaci nas iz igre odnosno prebaci u glavni meni svih igara
        return;
    }
    event->accept();
}
