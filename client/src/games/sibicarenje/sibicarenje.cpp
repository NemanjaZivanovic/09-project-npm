#include "headers/games/sibicarenje/sibicarenje.h"
#include "headers/games/sibicarenje/sibicarenjeinstructions.h"

#include <QPen>
#include <QColor>
#include <QImage>
#include <QRandomGenerator>
#include <QThread>
#include <QGraphicsScene>
#include <QPainter>

Sibicarenje::Sibicarenje(QGraphicsView* parent, GameItem::GameId gameId)
    : Game(parent, gameId)
{
    scene()->setSceneRect(-scene()->width()/2,
                          -scene()->height()/2,
                          scene()->width(),
                          scene()->height());
    m_box1 = m_box2 = m_box3 = nullptr;
    m_ball = nullptr;
    m_levelLCD = nullptr;
    m_buttonStartRotations = nullptr;
    m_returnToMenuButton = m_exitButton = nullptr;
    m_hitMsg = m_missMsg = m_gameOverMsg = m_endGameMsg = nullptr;

    setGameMenu();

    const GameClient* client = gameClient();
    connect(client, &GameClient::gameInterrupted, this, &Sibicarenje::onGameInterrupted);
    connect(client, &GameClient::connected, this, &Sibicarenje::connectedToServer);
    connect(client, &GameClient::disconnected, this, &Sibicarenje::disconnectedFromServer);
    connect(client, &GameClient::error, this, &Sibicarenje::onConnectionError);
    connect(client, &GameClient::gameInit, this, &Sibicarenje::gameInit);
    connect(client, &GameClient::opponentMove, this, &Sibicarenje::opponentMove);
    connect(client, &GameClient::newGameOffer, this, &Sibicarenje::onNewGameOffer);
}

void Sibicarenje::setGameMenu()
{
    scene()->clear();
    setBackground();

    m_singlePlayerButton = new SibicarenjeButton(-180,-80-80,180, 75, "single player");
    scene()->addItem((m_singlePlayerButton));
    connect(m_singlePlayerButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::setGameScene);

    m_multiplayerButton = new SibicarenjeButton(0,-80,180, 75, "multiplayer");
    scene()->addItem(m_multiplayerButton);
    connect(m_multiplayerButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::connectToServer);
    connect(m_multiplayerButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::unavailableServer);

    m_instructionsButton = new SibicarenjeButton(-180,80-80,180, 75, "instructions");
    scene()->addItem(m_instructionsButton);
    connect(m_instructionsButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::setInstructionsScene);

    m_exitButton = new SibicarenjeButton(0,2*80-80,180, 75, "exit");
    scene()->addItem(m_exitButton);
}

void Sibicarenje::setGameScene()
{
    scene()->clear();
    setBackground();

    // m_box1, m_box2, m_box3 dobijaju informaciju o tome na mestu koje
    // pocetne kutije se nalaze, to ce biti azurirano kadgod se zavrsi
    // rotacija kutije
    m_box1 = new Box(CurrentBox::FirstBox);
    scene()->addItem(m_box1);
    m_box2 = new Box(CurrentBox::SecondBox);
    scene()->addItem(m_box2);
    m_box3 = new Box(CurrentBox::ThirdBox);
    scene()->addItem(m_box3);

    // na pocetku je lopta uvek u drugoj kutiji
    m_ball = new Ball(m_box2);

    m_levelLCD = new QLCDNumber();
    m_levelLCD->display(1);
    m_levelLCD->setMinimumSize(80,50);
    m_levelLCD->move(scene()->width()/3+40, -scene()->height()/3-80);
    scene()->addWidget(m_levelLCD);

    m_buttonStartRotations = new QPushButton("start game");
    m_buttonStartRotations->move(scene()->width()/3, scene()->height()/3);
    m_buttonStartRotations->setMinimumSize(120,50);
    scene()->addWidget(m_buttonStartRotations);

    m_exitButton = new SibicarenjeButton(0,2*80-80,100, 50, "exit");
    m_exitButton->moveBy(-scene()->width()/3-120, scene()->height()/3-80);
    scene()->addItem(m_exitButton);

    m_returnToMenuButton = new SibicarenjeButton(0,0,100, 50, "menu");
    m_returnToMenuButton->moveBy(-scene()->width()/3-120, -scene()->height()/3-80);
    scene()->addItem(m_returnToMenuButton);

    connect(m_box1, &Box::showBall, this, &Sibicarenje::showBall);
    connect(m_box2, &Box::showBall, this, &Sibicarenje::showBall);
    connect(m_box3, &Box::showBall, this, &Sibicarenje::showBall);

    connect(m_box1, &Box::hideBall, this, &Sibicarenje::hideBall);
    connect(m_box2, &Box::hideBall, this, &Sibicarenje::hideBall);
    connect(m_box3, &Box::hideBall, this, &Sibicarenje::hideBall);

    // zelimo da kutije mogu da blokiraju jedna druge, ovde samo povezujemo odgovarajuce
    // signale i slotove iz klase Box
    connect(m_box1, &Box::sendBlockOpeningBoxSignal, m_box1, &Box::blockOpeningBox);
    connect(m_box1, &Box::sendBlockOpeningBoxSignal, m_box2, &Box::blockOpeningBox);
    connect(m_box1, &Box::sendBlockOpeningBoxSignal, m_box3, &Box::blockOpeningBox);
    connect(m_box2, &Box::sendBlockOpeningBoxSignal, m_box1, &Box::blockOpeningBox);
    connect(m_box2, &Box::sendBlockOpeningBoxSignal, m_box2, &Box::blockOpeningBox);
    connect(m_box2, &Box::sendBlockOpeningBoxSignal, m_box3, &Box::blockOpeningBox);
    connect(m_box3, &Box::sendBlockOpeningBoxSignal, m_box1, &Box::blockOpeningBox);
    connect(m_box3, &Box::sendBlockOpeningBoxSignal, m_box2, &Box::blockOpeningBox);
    connect(m_box3, &Box::sendBlockOpeningBoxSignal, m_box3, &Box::blockOpeningBox);

    connect(this, &Sibicarenje::rotateBox, m_box1, &Box::prepareNewRotation);
    connect(this, &Sibicarenje::rotateBox, m_box2, &Box::prepareNewRotation);
    connect(this, &Sibicarenje::rotateBox, m_box3, &Box::prepareNewRotation);

    connect(m_box1, &Box::finishedRotating, this, &Sibicarenje::prepareNewRotation);
    connect(m_box2, &Box::finishedRotating, this, &Sibicarenje::prepareNewRotation);
    connect(m_box3, &Box::finishedRotating, this, &Sibicarenje::prepareNewRotation);

    connect(m_box1, &Box::chosenBox, this, &Sibicarenje::checkPlayerChoice);
    connect(m_box2, &Box::chosenBox, this, &Sibicarenje::checkPlayerChoice);
    connect(m_box3, &Box::chosenBox, this, &Sibicarenje::checkPlayerChoice);

    // zelimo da i iz klase Sibicarenje mozemo da (od)blokiramo kutije, potrebno je samo da se
    // povezemo na signal sendBlockOpeningBoxSignal bar jedne kutije jer ce ona svakako
    // svima ostalima proslediti da treba da se (od)blokiraju
    connect(this, &Sibicarenje::blockOpeningBox, m_box1, &Box::sendBlockOpeningBoxSignal);

    connect(m_buttonStartRotations, &QPushButton::clicked, this, &Sibicarenje::prepareNewRotation);
    connect(m_returnToMenuButton, &SibicarenjeButton::clicked, this, &Sibicarenje::returnToMenu);
    connect(m_exitButton, &SibicarenjeButton::clicked, this, &Sibicarenje::closeServerConnection);

    m_hitMsg = scene()->addText("hit!");
    m_hitMsg->moveBy(-40, -scene()->height()/3-60);
    m_hitMsg->setScale(3);
    m_hitMsg->hide();

    m_missMsg = scene()->addText("miss!");
    m_missMsg->moveBy(-60, -scene()->height()/3-60);
    m_missMsg->setScale(3);
    m_missMsg->hide();

    m_gameOverMsg = scene()->addText("game over");
    m_gameOverMsg->moveBy(-130, scene()->height()/3-20);
    m_gameOverMsg->setScale(3);
    m_gameOverMsg->hide();

    m_endGameMsg = scene()->addText("all levels passed!");
    m_endGameMsg->moveBy(-180, scene()->height()/3-20);
    m_endGameMsg->setScale(3);
    m_endGameMsg->hide();

    m_box2->openBox();

    m_numberOfRotations = 10;
    m_currentRotationNumber = 0;
    // previousRotationValue se postavlja na 100, u principu je samo bitno
    // da na pocetku bude bilo koja celobrojna vrednost van intervala [0,5)
    m_previousRotationValue = 100;

    m_level = 0;
    m_restartGame = false;

    // zelimo da kutije budu blokirane na pocetku
    emit blockOpeningBox(true);
}


void Sibicarenje::setWaitingScene()
{
    scene()->clear();
    setBackground();

    m_returnToMenuButton = new SibicarenjeButton(-125, 0, 100, 50, "menu");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::returnToMenu);

    m_exitButton = new SibicarenjeButton(25, 0, 100, 50, "exit");
    scene()->addItem(m_exitButton);
    connect(m_exitButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::closeServerConnection);

    auto waitingMsg = scene()->addText("waiting for opponent");
    waitingMsg->moveBy(-210, -scene()->height()/3 + 80);
    waitingMsg->setScale(3);
}

void Sibicarenje::setNewGameScene()
{
    scene()->clear();
    setBackground();

    auto newGameButton = new SibicarenjeButton(-125, 0, 100, 50, "yes");
    scene()->addItem(newGameButton);
    connect(newGameButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::connectedToServer);

    m_returnToMenuButton = new SibicarenjeButton(25, 0, 100, 50, "no");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::returnToMenu);

    auto newGameOfferMsg = scene()->addText("new game?");
    newGameOfferMsg->moveBy(-120, -scene()->height()/3 + 80);
    newGameOfferMsg->setScale(3);
}

void Sibicarenje::setInstructionsScene()
{
    scene()->clear();
    setBackground();

    m_returnToMenuButton = new SibicarenjeButton(-400, 200, 100, 50, "back");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::returnToMenu);

    scene()->addItem(new SibicarenjeInstructions);

    Box* box = new Box(CurrentBox::FirstBox);
    scene()->addItem(box);
}

void Sibicarenje::chooseNextRotation(bool firstRotation)
{
    // iznudjeno resenje za situaciju kada na pocetku svake serije rotacija treba napraviti
    // kratku pauzu izmedju zatvaranja kutije i zapocinjanja serije rotacija
    // funkcija chooseNextRotation ce, ako je u pitanju prva rotacija u seriji rotacija, poslati
    // prvoj kutiji rotateSide = NONE, sto sluzi samo da bi se azurirala slika zatvorene kutije;
    // nije najcistije resenje, ali drugi pokusaji nisu uspeli
    if(firstRotation==true){
        emit rotateBox(CurrentBox::FirstBox, NONE, 0);
        return;
    }

    quint32 value = QRandomGenerator::global()->bounded(0,5);

    // ne zelimo da dobijemo dva uzastopna ista value-a, tj dve
    // uzastopne iste rotacije, pa generisemo novu vrednost sve
    // dok ne dobijemo razlicitu
    while(value == m_previousRotationValue)
        value = QRandomGenerator::global()->bounded(0,5);

    m_previousRotationValue = value;
    // 0 zameni prvu i drugu kutiju 1 2 3 -> 2 1 3
    // 1 drugu i trecu              1 2 3 -> 1 3 2
    // 2 trecu i prvu               1 2 3 -> 3 2 1
    // 3 prve dve i trecu           1 2 3 -> 3 1 2
    // 4 druge dve i prvu           1 2 3 -> 2 3 1
    switch (value) {
        case 0:
            emit rotateBox(CurrentBox::FirstBox, RIGHT_SIDE, m_level);
            emit rotateBox(CurrentBox::SecondBox, LEFT_SIDE, m_level);
            break;
        case 1:
            emit rotateBox(CurrentBox::SecondBox, RIGHT_SIDE, m_level);
            emit rotateBox(CurrentBox::ThirdBox, LEFT_SIDE, m_level);
            break;
        case 2:
            emit rotateBox(CurrentBox::ThirdBox, RIGHT_SIDE, m_level);
            emit rotateBox(CurrentBox::FirstBox, LEFT_SIDE, m_level);
            break;
        case 3:
            emit rotateBox(CurrentBox::FirstBox, RIGHT_SIDE, m_level);
            emit rotateBox(CurrentBox::SecondBox, RIGHT_SIDE, m_level);
            emit rotateBox(CurrentBox::ThirdBox, RIGHT_SIDE, m_level);
            break;
        case 4:
            emit rotateBox(CurrentBox::FirstBox, LEFT_SIDE, m_level);
            emit rotateBox(CurrentBox::SecondBox, LEFT_SIDE, m_level);
            emit rotateBox(CurrentBox::ThirdBox, LEFT_SIDE, m_level);
            break;
    }
}

void Sibicarenje::prepareNewRotation()
{
    // ako se bar jedna kutija i dalje rotira ne zelimo da pocnemo novu rotaciju
    if(m_box1->isRotating() || m_box2->isRotating() || m_box3->isRotating())
        return;

    if(m_restartGame == true){

        m_restartGame = false;
        m_box1->closeBox();
        m_box2->closeBox();
        m_box3->closeBox();

        m_level = 0;
        m_levelLCD->display(1);

        m_missMsg->hide();
        m_gameOverMsg->hide();
        m_endGameMsg->hide();

        emit blockOpeningBox(true);
    }

    // ako je u pitanju prva rotacija u seriji rotacija, skrivamo
    // poruku o pogotku (ako je bilo takve poruke), povecavamo nivo
    // zatvaramo otvorenu kutiju i onemogucavamo da se pritiska dugme
    // za rotiranje kutija
    if(m_currentRotationNumber == 0){

        m_hitMsg->hide();

        if(m_level != 0)
            increaseLCD();
        m_level++;

        if(m_box1->isOpened())
            m_box1->closeBox();
        else if(m_box2->isOpened()){
            m_box2->closeBox();
        }
        else if(m_box3->isOpened()){
            m_box3->closeBox();
        }

        m_buttonStartRotations->setEnabled(false);
        m_buttonStartRotations->setText("rotating...");
        emit blockOpeningBox(true);
    }

    m_currentRotationNumber += 1;
    if(m_currentRotationNumber <= m_numberOfRotations){
        QThread::msleep(80);
        // postavljamo true ako je prva rotacija u pitanju, inace false
        // pogledati pocetni deo chooseNextRotation za objasnjenje zasto
        m_currentRotationNumber==1 ? chooseNextRotation(true) : chooseNextRotation(false);
    }
    else{
        // ako je serija rotacija zavrsena, odblokiramo kutije kako bi igrac mogao da bira
        emit blockOpeningBox(false);
        // restartujemo vrednosti koje su nam bitne za rotiranje
        m_currentRotationNumber = 0;
        m_previousRotationValue = 100;
        m_buttonStartRotations->setText("choose a box");
    }
}

void Sibicarenje::checkPlayerChoice(CurrentBox chosenBox)
{
    Box* boxWithTheBall;
    Box* playerChoiceBox;

    if(m_box1->hasTheBall())
        boxWithTheBall = m_box1;
    else if(m_box2->hasTheBall())
        boxWithTheBall = m_box2;
    else
        boxWithTheBall = m_box3;

    if(m_box1->currentBox() == chosenBox)
        playerChoiceBox = m_box1;
    else if(m_box2->currentBox() == chosenBox)
        playerChoiceBox = m_box2;
    else
        playerChoiceBox = m_box3;

    if(boxWithTheBall == playerChoiceBox){ // igrac je pogodio kutiju u kojoj je loptica
        m_hitMsg->show();
        m_buttonStartRotations->setEnabled(true);
        if(m_level==15){ // igra je zavrsena
            if(m_multiplayerModeActive==false){ // single player deo
                m_buttonStartRotations->setText("restart");
                m_restartGame = true;
                m_endGameMsg->show();
            }
            else{ // multiplayer deo
                m_buttonStartRotations->setText("waiting");
                m_buttonStartRotations->setEnabled(false);
                m_endGameMsg = scene()->addText("waiting for opponent");
                m_endGameMsg->moveBy( -210, scene()->height()/3-20);
                m_endGameMsg->setScale(3);
                QJsonObject msg;
                msg["level"] = m_level+1;
                sendMove(msg, Invalid);
                m_gameFinished = true;
            }
        }
        else // nije zavrsena igra, treba se spremiti za naredni nivo
            m_buttonStartRotations->setText("next level");
    }
    else{ // igrac je odabrao pogresnu kutiju
        m_missMsg->show();
        boxWithTheBall->openBox();

        if(m_multiplayerModeActive==true){
            QJsonObject msg;
            msg["level"] = m_level;
            sendMove(msg, Invalid);
            m_gameFinished = true;
            m_endGameMsg = scene()->addText("waiting for opponent");
            m_endGameMsg->moveBy( -210, scene()->height()/3-20);
            m_endGameMsg->setScale(3);
        } else{
            m_buttonStartRotations->setEnabled(true);
            m_buttonStartRotations->setText("restart");
            m_restartGame = true;
            m_gameOverMsg->show();

        }
    }
}

void Sibicarenje::showBall()
{
    m_ball->show();
}

void Sibicarenje::hideBall()
{
    m_ball->hide();
}

void Sibicarenje::increaseLCD()
{
    m_levelLCD->display(m_levelLCD->intValue()+1);
}

void Sibicarenje::returnToMenu()
{
    setGameMenu();
    closeServerConnection();
}

void Sibicarenje::showMessage(QString msg)
{
    m_notification.setMessage(msg);
    auto x = scene()->views()[0]->mapFromGlobal(QPoint(0,0)).x();
    auto y = scene()->views()[0]->mapFromGlobal(QPoint(0,0)).y();
    m_notification.setGeometry(-x + scene()->width()/2 - m_notification.width()/2,
                               -y + scene()->height()/2 - m_notification.height()/2,
                               m_notification.width(), m_notification.height());
    m_notification.show();
}

void Sibicarenje::setBackground()
{
    scene()->addItem(new SibicarenjeBackground);
}

void Sibicarenje::connectedToServer()
{
    // Funkcija se pokrece se kada se povezemo sa serverom

    initNewGame();

    m_multiplayerModeActive = true;
    setWaitingScene();
}

void Sibicarenje::disconnectedFromServer()
{
    /*
     * Metod ce biti pozvan ukoliko dodje do
     * "regularnog" prekida konekcije
     * (mi prekidamo vezu, server se ugasio i sl).
    */
    if(m_multiplayerModeActive == true){
        returnToMenu();
        showMessage("Disconnected from server");
    }
}

void Sibicarenje::gameInit(int player, int gameIndex)
{
    /*
     * Vec cekamo u redu za igru, server je uspeo
     * da nas upari sa nekim igracem.
     * Odredio je ko je koji igrac.
     *
     * Ulazni argumenti sluze samo da se proslede
     * verziji funkcije od nadklase, ona sve podesava.
     *
    */

    Game::gameInit(player, gameIndex);

    setGameScene();
}

void Sibicarenje::opponentMove(const QJsonObject &move)
{
    /*
     * Drugi igrac nam je prosledio svoj potez,
     * ovde reagujemo na njega.
    */

    /*
     * Napomena:
     * Poruka ne sme da sadrzi polja 'type', 'outcome', 'gameIndex'.
     * To su kontrolni podaci i podesice se interno pozivom sendMove.
    */

    // ako je nivo bio -1, tj do sad nismo dobili informaciju o tome do kog je nivoa
    // stigao protivnik, azuriramo vrednost
    if(m_opponentLevel==-1)
        m_opponentLevel = move["level"].toInt();
    // ako je zavrsena igra, o tome se obavestava server
    if(m_gameFinished == true){
        QJsonObject msg;
        msg["end game"] = 2356;
        sendMove(msg, Player1);
    }
}

void Sibicarenje::onNewGameOffer()
{
    /*
     * Igra se je zavrsila na regularan nacin.
     * Server nas je stavio u opstu listu klijenata
     * i pita nas da li zelimo da igramo opet.
    */
    QString temp = "";
    if(m_level > m_opponentLevel)
        temp += "You won ";
    else if(m_level < m_opponentLevel)
        temp += "You lost ";
    else
        temp += "Draw ";
    temp += QString::number(m_level);
    temp += ":";
    temp += QString::number(m_opponentLevel);

    showMessage(temp);
    setNewGameScene();
}

void Sibicarenje::onGameInterrupted()
{
    /*
     * Igra je bila u toku i drugi igrac je
     * prekinuo vezu. Server nas je vratio u red za
     * cekanje i obavestava nas o tome.
    */
    returnToMenu();
    showMessage("opponent disconnected");
}

void Sibicarenje::onConnectionError(QAbstractSocket::SocketError socketError)
{
    /*
     * Ovde reagujemo na greske sa konekcijom
     * koje su u grupi "neregularnih".
     *
     * Obavezno prvo pozvati metod error iz nadklase
     * i proslediti mu argument. On ce generisati poruku
     * o gresci koju mozemo dobiti getter metodom ispod.
     *
     * Napomena: cak i kada npr. mi sami prekinemo vezu,
     * to se smatra greskom. Medjutim, funkcija nadklase
     * ce u takvim "regularnim" slucajevima postaviti errorMessage
     * na nullptr - stoga provera toga ispod.
     *
     * Ovakvi slucajevi, kao i kada mi sami prekinemo vezu,
     * obradjuju se u slotu disconnectedFromServer.
     *
     * Zato bi ova funkcija trebalo da sadrzi naredne redove i
     * nakon toga obraditi te ostale greske.
    */

    Game::error(socketError);
    QString errorMsg = errorMessage();
    if(errorMsg == nullptr) {
        return;
    }
    returnToMenu();
    showMessage("connection error");
}

void Sibicarenje::unavailableServer()
{
    scene()->clear();
    setBackground();

    m_returnToMenuButton = new SibicarenjeButton(-125, 0, 100, 50, "menu");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &SibicarenjeButton::clicked,
            this, &Sibicarenje::returnToMenu);

    m_exitButton = new SibicarenjeButton(25, 0, 100, 50, "exit");
    scene()->addItem(m_exitButton);

    auto serverUnavailableMsg = scene()->addText("server unavailable");
    serverUnavailableMsg->moveBy(-180, -scene()->height()/3 + 80);
    serverUnavailableMsg->setScale(3);
}

void Sibicarenje::closeServerConnection()
{
    if(m_multiplayerModeActive == true){
        m_multiplayerModeActive = false;
        emit disconnectFromServer();
    }
}
