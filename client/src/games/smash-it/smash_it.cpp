﻿#include "headers/games/smash-it/smash_it.h"
#include "headers/games/smash-it/smash_itinstructions.h"

#include <QVector>
#include <QGraphicsScene>
#include <numeric>
#include <algorithm>
#include <random>

Smash_It::Smash_It(QGraphicsView* parent, GameItem::GameId gameId)
    : Game(parent, gameId)
{
    m_rangeOfFieldIndexes = nullptr;
    m_highScore = 0;
    m_multiplayerModeActive = false;
    m_gameStarted = false;
    m_gameEnded = false;

    scene()->setBackgroundBrush(Qt::cyan);
    setGameMenu();

    const GameClient* client = gameClient();
    connect(client, &GameClient::gameInterrupted, this, &Smash_It::onGameInterrupted);
    connect(client, &GameClient::connected, this, &Smash_It::connectedToServer);
    connect(client, &GameClient::disconnected, this, &Smash_It::disconnectedFromServer);
    connect(client, &GameClient::error, this, &Smash_It::onConnectionError);
    connect(client, &GameClient::gameInit, this, &Smash_It::gameInit);
    connect(client, &GameClient::opponentMove, this, &Smash_It::opponentMove);
    connect(client, &GameClient::newGameOffer, this, &Smash_It::onNewGameOffer);
}

Smash_It::~Smash_It()
{
    delete m_rangeOfFieldIndexes;
}

void Smash_It::setGameScene()
{
    // Ovde se predlaze postavljanje inicijalne scene.
    m_fields = QVector<Field*>();
    // koordinate origin-a tj gde se svako polje na inicijalno nalazi
    // ono ce biti pomereno za moveVector vise puta u zavisnosti od toga
    // gde se nalazi na terenu
    QPointF origin(-367.5, -225);
    QPointF moveVector(32.5, -55);
    // potrebna nam je informacija o tome da li je u pitanju paran ili neparan red
    // kako bi polje pomerili manje ili vise ulevo (kako bi se polja lepo namestila)
    bool evenRow = true;
    // pocetni brojevi za prvo polje gore levo
    qint16 antiDiagonalNumber = 0;
    qint16 mainDiagonalNumber = 4;
    // broj redova terena
    qint16 numberOfRows = 9;
    m_antiDiagonalLimit = antiDiagonalNumber;

    for(int rowNumber=0; rowNumber < numberOfRows; rowNumber++){
        // zelimo da je red pomeren u levo samo ako je even false tj ako je neparan red
        // ceo red se pomera po x koordinati u zavisnosti od parnosti reda, dok se po y
        // koordinati pomera u svakom redu za y koordinatu moveVector-a
        // sa createRow pravimo celi red
        createRow(origin.x()-!evenRow*moveVector.x(),
                  origin.y()-rowNumber*moveVector.y(),
                  evenRow, antiDiagonalNumber, mainDiagonalNumber, rowNumber);
        evenRow = !evenRow;
        // kad je paran red, prvo polje u redu treba da ima za 1 veci broj
        // sporedne dijagonale, a za jedan manji broj glavne dijagonale
        evenRow == true ? antiDiagonalNumber++ : mainDiagonalNumber--;
    }
    connect(this, &Smash_It::changeStatusAlongTheDirection,
            this, &Smash_It::forceChangeAlongTheDirection);

    for(Field* field: m_fields){
        connect(this, &Smash_It::changeStatus, field, &Field::changeStatus);
        connect(field, &Field::changeStatusAlongTheDirection,
                this, &Smash_It::changeStatusAlongTheDirection);
        connect(this, &Smash_It::forceChangeAlongTheDirection,
                field, &Field::onForceChangeAlongTheDirection);
        connect(field, &Field::addPoints, this, &Smash_It::addPoints);
        connect(field, &Field::deductPoints, this, &Smash_It::deductPoints);
        connect(field, &Field::increaseSecondsLeft, this, &Smash_It::increaseSecondsLeft);
        connect(field, SIGNAL(decreaseSecondsLeft(qint32)), this, SLOT(decreaseSecondsLeft(qint32)));
        connect(field, &Field::eraseAllPoints, this, &Smash_It::eraseAllPoints);
    }

    setDefaultGameParameters();

    // samo jednom izracunavamo vektor indeksa polja, buduci da je informacija ista
    // pa nema potrebe iznova svaki put izracunavati kad se postavlja scena igre
    if(m_rangeOfFieldIndexes == nullptr){
        m_rangeOfFieldIndexes = new QVector<qint16>(m_fields.size());
        std::iota(m_rangeOfFieldIndexes->begin(), m_rangeOfFieldIndexes->end(), 0);
    }

    m_pointsDisplay = scene()->addText(
                    QString::fromStdString("Points: " + std::to_string(m_totalPoints)));
    m_pointsDisplay->moveBy( -170, -scene()->height()/3-100);
    m_pointsDisplay->setScale(2);

    if(m_multiplayerModeActive == false){
        m_highScoreDisplay = scene()->addText(
                    QString::fromStdString("HighScore: " + std::to_string(m_highScore)));
        m_highScoreDisplay->moveBy( -175, +scene()->height()/3+50);
        m_highScoreDisplay->setScale(2);

        m_startGameButton = new Smash_ItButton(scene()->width()/2.5,250,80,70, "start game");
        scene()->addItem(m_startGameButton);
        connect(m_startGameButton, &Smash_ItButton::startGame, this, &Smash_It::startGame);
        connect(m_startGameButton, &Smash_ItButton::restartGame, this, &Smash_It::restartGame);
    }
    else{
        m_opponentsPoints = 0;
        m_opponentFinished = false;
        m_winningAnimationStarted=false;

        m_opponentsPointsDisplay = scene()->addText(
                    QString::fromStdString("Opponent's points: " + std::to_string(m_opponentsPoints)));
        m_opponentsPointsDisplay->moveBy( -175, +scene()->height()/3+50);
        m_opponentsPointsDisplay->setScale(2);
    }

    m_clickEffectDisplay = scene()->addText("");
    m_clickEffectDisplay->moveBy( 70, -scene()->height()/3-100);
    m_clickEffectDisplay->setScale(2);

    m_secondsLeftDisplay = scene()->addText(
                QString::fromStdString("Time left: " + std::to_string(m_secondsLeft)));
    m_secondsLeftDisplay->moveBy( 295, -scene()->height()/3-100);
    m_secondsLeftDisplay->setScale(2);

    m_returnToMenuButton = new Smash_ItButton(-scene()->width()/2+20,
                                      -scene()->height()/3 - 100, 80, 70, "menu");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &Smash_ItButton::returnToMenu,
            this, &Smash_It::returnToMenu);

    m_exitButton = new Smash_ItButton(-scene()->width()/2+20, 250, 80, 70, "exit");
    scene()->addItem(m_exitButton);
    connect(m_exitButton, &Smash_ItButton::endGame, this, &Smash_It::closeServerConnection);
    connect(m_exitButton, &Smash_ItButton::endGame, this, &Smash_It::endGame);

    if(m_multiplayerModeActive == true)
        startGame();
}

void Smash_It::createRow(qreal x, qreal y, bool evenRow, qint16 antiDiagonalNumber, qint16 mainDiagonalNumber, qint16 rowNumber)
{
    // vektor pomeraja polja
    QPointF moveVector(65,0);
    // broj polja u redu je 12 ili 13 u zavisnosti od toga da li je red paran ili neparan
    qint16 rowElementsNumber = 12;
    if(evenRow == false)
        rowElementsNumber+=1;
    for(int posInRow = 0; posInRow < rowElementsNumber; posInRow++){
        m_fields.push_front(new Field(antiDiagonalNumber, mainDiagonalNumber,
                                       rowNumber, posInRow, Status::NORMAL_NOT_ACTIVE));
        scene()->addItem(m_fields.front());
        m_fields.front()->setParent(this);
        // u zavisnosti od pozicije u redu se menja x koordinata, y koordinata je
        // ista za celi red
        m_fields.front()->moveBy(x+posInRow*moveVector.x(), y+moveVector.y());
        antiDiagonalNumber++;
        mainDiagonalNumber++;
        if(m_antiDiagonalLimit < antiDiagonalNumber)
            m_antiDiagonalLimit = antiDiagonalNumber;
    }

}

void Smash_It::changeStatusOfFields()
{
    QVector<qint16> fieldIndexes;
    size_t sampleSize = 10;
    // koristimo prethodno napunjeni m_rangeOfFieldIndexes sa indeksima polja
    // i zatim vektor fieldIndexes punimo sa 10 pseudoslucajno odabranih vrednosti
    std::sample(
        m_rangeOfFieldIndexes->begin(), m_rangeOfFieldIndexes->end(),
        std::back_inserter(fieldIndexes),
        sampleSize,
        std::mt19937{std::random_device{}()}
    );

    // emitujemo signal changeStatus za izabrana polja sa pseudoslucajnim odabranim statusom
    for(auto index: fieldIndexes)
        emit changeStatus(m_fields.at(index), randomlyPickStatus(), 2500);
}

Status Smash_It::randomlyPickStatus()
{
    // moguci statusi za polja su ova, ne zelimo da mozemo da prebacimo polja
    // odmah u sve moguce statuse, npr MINUS_10_POINTS_PRESSED se aktivira samo
    // kada je polje sa statusom MINUS_10_POINTS_NOT_PRESSED pritisnuto
    QVector<Status> possibleStatus{MINUS_10_POINTS_NOT_PRESSED, PLUS_5_POINTS_NOT_PRESSED, NO_EFFECT,
                                   DISABLE_ANTI_DIAGONAL, DISABLE_MAIN_DIAGONAL,
                                   DISABLE_ROW, DISABLE_COLUMN, ZERO_POINTS};
    // verovatnoce pojavljivanja gornjih polja
    QVector<qreal>probabilities = { 41, 46, 1, 2, 2, 2, 2, 4 };
    if(m_multiplayerModeActive == false){
        // u multiplayer varijanti igre nemamo polja sa statusom PLUS_5_SECONDS i
        // MINUS_10_SECONDS kako bi igre bile vremenski sinhronizovane
        possibleStatus.push_back(PLUS_5_SECONDS);
        possibleStatus.push_back(MINUS_10_SECONDS);
        probabilities[0] -= 1;
        probabilities[1] -= 2;
        probabilities.push_back(1);
        probabilities.push_back(2);
    }
    // objekat potreban za nextInt
    std::mt19937 helpObject( std::random_device{}() ) ;
    std::discrete_distribution<int> nextInt( probabilities.begin(), probabilities.end() ) ;

    // nextInt(helpObject) bira neki od brojeva iz intervala [0,n) gde je n najveci moguci
    // integer sa verovatnocama probabilities (posle poslednjeg broja u vektoru ostali su
    // implicitno 0); retka polja imaju malu verovatnocu da se pojave;
    // kad se odabere jedan od tih brojeva, uzimamo ga kao indeks vektora possibleStatus
    // i taj status vracamo kao povratnu vrednost funkcije
    return possibleStatus.at(nextInt(helpObject));
}

void Smash_It::setInstructionsScene()
{
    scene()->clear();

    auto status = {NORMAL_NOT_ACTIVE, NO_EFFECT, PLUS_5_POINTS_NOT_PRESSED, MINUS_10_POINTS_NOT_PRESSED,
                  ZERO_POINTS, DISABLE_ROW, DISABLE_COLUMN, DISABLE_MAIN_DIAGONAL, DISABLE_ANTI_DIAGONAL,
                   PLUS_5_SECONDS, MINUS_10_SECONDS};

    auto fieldDescriptions = QVector{
        "normal field look",
        "does nothing",
        "+5 points",
        "-10 points",
        "returns points to 0",
        "disables all elements in the same row\n+10 points",
        "disables all elements in the same column\n+10 points",
        "disables all elements on the same main diagonal\n+10 points",
        "disables all elements on the same anti diagonal\n+10 points",
        "+5 seconds\n(only single player)",
        "-10 seconds\n(only single player)"
    };

    QPointF origin(-380, -280);
    QPointF moveVector(32.5, -55);
    int row = 0;
    int index = 0;
    bool secondColumn = false;
    for(auto status: status){
        Field* field = new Field(0, 0, 0, 0, status);
        field->setParent(this);
        // x koordinata polja zavisi od toga u kojoj su koloni u instructions delu
        // prva kolona ima 5 polja
        field->moveBy(origin.x()+moveVector.x() + secondColumn*300, origin.y()-2*row*moveVector.y());
        scene()->addItem(field);
        auto fieldDescription = scene()->addSimpleText(fieldDescriptions[index]);
        // sva polja u drugoj koloni imaju opis u dva reda, stoga je potrebno da pocetak pomerimo
        // malo po y koordinati
        fieldDescription->moveBy(field->pos().x()+moveVector.x()+10, field->pos().y()-10-secondColumn*10);

        if(row == 4 && secondColumn == false)
            secondColumn = true, row = -1;

        row++;
        index++;
    }

    // objekat klase Smash_ItInstructions dodajemo posle polja kako bi smo onemogucili
    // da se aktivira normalno ponasanje polja (iz igre)
    scene()->addItem(new Smash_ItInstructions);

    // dugme za povratak u meni dodajemo posle objekta klase Smash_ItInstructions
    // kako dogadjaj ne bi bio blokiran
    m_returnToMenuButton = new Smash_ItButton(-400, 250, 100, 50, "back");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &Smash_ItButton::returnToMenu,
            this, &Smash_It::returnToMenu);
}

void Smash_It::setWaitingScene()
{
    scene()->clear();

    m_returnToMenuButton = new Smash_ItButton(-125, 0, 100, 50, "menu");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &Smash_ItButton::returnToMenu,
            this, &Smash_It::returnToMenu);

    m_exitButton = new Smash_ItButton(25, 0, 100, 50, "exit");
    scene()->addItem(m_exitButton);
    connect(m_exitButton, &Smash_ItButton::endGame,
            this, &Smash_It::closeServerConnection);

    auto waitingMsg = scene()->addText("waiting for opponent");
    waitingMsg->moveBy(-210, -scene()->height()/3 + 80);
    waitingMsg->setScale(3);
}

void Smash_It::setNewGameScene()
{
    scene()->clear();

    m_returnToMenuButton = new Smash_ItButton(25, 0, 100, 50, "no");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &Smash_ItButton::returnToMenu,
            this, &Smash_It::returnToMenu);

    auto newGameButton = new Smash_ItButton(-125, 0, 100, 50, "yes");
    scene()->addItem(newGameButton);
    connect(newGameButton, &Smash_ItButton::multiPlayerMode,
            this, &Smash_It::connectedToServer);

    auto waitingMsg = scene()->addText("new game?");
    waitingMsg->moveBy(-120, -scene()->height()/3 + 80);
    waitingMsg->setScale(3);
}

void Smash_It::closeServerConnection()
{
    if(m_multiplayerModeActive == true){
        m_multiplayerModeActive = false;
        emit disconnectFromServer();
    }
}

void Smash_It::showMessage(QString msg)
{
    m_notification.setMessage(msg);
    auto x = scene()->views()[0]->mapFromGlobal(QPoint(0,0)).x();
    auto y = scene()->views()[0]->mapFromGlobal(QPoint(0,0)).y();
    m_notification.setGeometry(-x + scene()->width()/2 - m_notification.width()/2,
                               -y + scene()->height()/2 - m_notification.height()/2,
                               m_notification.width(), m_notification.height());
    m_notification.show();
}

void Smash_It::setOpponentsPoints(qint32 points)
{
    m_opponentsPoints = points;
    m_opponentsPointsDisplay->setPlainText("Opponent's points: " +
                              QString::number(m_opponentsPoints));

}

bool Smash_It::getHighScoreBeatenAnimation() const
{
    return m_highScoreBeatenAnimation;
}

void Smash_It::startGame()
{
    if(m_gameStarted == true)
        return;
    m_gameStarted = true;

    changeStatusOfFields();

    m_timerChangeStatusOfFields = new QTimer();
    connect(m_timerChangeStatusOfFields, &QTimer::timeout, this, &Smash_It::changeStatusOfFields);
    m_timerChangeStatusOfFields->start(2000);

    m_timerDecreaseSecondsLeft = new QTimer();
    // neophodna ovakva sintaksa zbog dve varijante slota decreaseSecondsLeft
    connect(m_timerDecreaseSecondsLeft, SIGNAL(timeout()), this, SLOT(decreaseSecondsLeft()));
    m_timerDecreaseSecondsLeft->start(1000);

    m_timerClickEffectDisplay = nullptr;
}

void Smash_It::restartGame()
{
    setDefaultGameParameters();
    m_startGameButton->setText("start game");
    m_pointsDisplay->setPlainText("Points: " + QString::number(m_totalPoints));
    m_highScoreDisplay->setPlainText("HighScore: " + QString::number(m_highScore));
    m_clickEffectDisplay->setPlainText("");
    m_secondsLeftDisplay->setPlainText("Time left: " + QString::number(m_secondsLeft));
    startGame();
}

void Smash_It::eraseAllPoints()
{
    m_totalPoints < 0 ? addPoints(-m_totalPoints) : deductPoints(m_totalPoints);
}

void Smash_It::returnToMenu()
{
    endGame();
    setGameMenu();
    closeServerConnection();
}

void Smash_It::endGame()
{
    if(m_gameStarted == false || m_gameEnded == true)
        return;
    m_gameStarted = false;
    m_gameEnded = true;

    if(m_timerChangeStatusOfFields!=nullptr){
        m_timerChangeStatusOfFields->stop();
        delete m_timerChangeStatusOfFields;
        m_timerChangeStatusOfFields=nullptr;
    }

    if(m_timerDecreaseSecondsLeft!=nullptr){
        m_timerDecreaseSecondsLeft->stop();
        delete m_timerDecreaseSecondsLeft;
        m_timerDecreaseSecondsLeft=nullptr;
    }

    if(m_timerClickEffectDisplay!=nullptr){
        m_timerClickEffectDisplay->stop();
        delete m_timerClickEffectDisplay;
        m_timerClickEffectDisplay=nullptr;
    }

    for(Field* field: m_fields)
        if(field->status() != NORMAL_NOT_ACTIVE)
            emit changeStatus(field, NORMAL_NOT_ACTIVE, 0);

    m_secondsLeftDisplay->setPlainText("Game ended");
    m_clickEffectDisplay->setPlainText("");

    if(m_secondsLeft <= 0){
        if(m_multiplayerModeActive == false){
            if(m_highScore < m_totalPoints){
                m_highScore = m_totalPoints;
                m_highScoreDisplay->setPlainText("NEW HIGHSCORE!");
                highScoreBeatenAnimation();
            }
        }else{ // multiplayer deo
            QJsonObject msg;
            msg["points"] = m_totalPoints;
            msg["seconds"] = m_secondsLeft;
            sendMove(msg, Invalid);
        }
    }
}

void Smash_It::setGameMenu()
{
    scene()->clear();

    m_singlePlayerButton = new Smash_ItButton(-180,-80-80,180, 75, "single player");
    scene()->addItem((m_singlePlayerButton));
    connect(m_singlePlayerButton, &Smash_ItButton::singlePlayerMode,
            this, &Smash_It::setGameScene);

    m_multiplayerButton = new Smash_ItButton(0,-80,180, 75, "multiplayer");
    scene()->addItem(m_multiplayerButton);
    connect(m_multiplayerButton, &Smash_ItButton::multiPlayerMode,
            this, &Smash_It::connectToServer);
    connect(m_multiplayerButton, &Smash_ItButton::multiPlayerMode,
            this, &Smash_It::unavailableServer);

    m_instructionsButton = new Smash_ItButton(-180,80-80,180, 75, "instructions");
    scene()->addItem(m_instructionsButton);
    connect(m_instructionsButton, &Smash_ItButton::instructions,
            this, &Smash_It::setInstructionsScene);

    m_exitButton = new Smash_ItButton(0,2*80-80,180, 75, "exit");
    scene()->addItem(m_exitButton);
}

void Smash_It::addPoints(qint32 points)
{
    setPoints(m_totalPoints + points);
    m_clickEffectDisplay->setDefaultTextColor(QColor(50,205,50));
    m_clickEffectDisplay->setPlainText("+" + QString::number(points));
    activateClickEffectDisplayTimer();
}

void Smash_It::deductPoints(qint32 points)
{
    setPoints(m_totalPoints-points);
    m_clickEffectDisplay->setDefaultTextColor(QColor(210,40,75));
    m_clickEffectDisplay->setPlainText("-" + QString::number(points));
    activateClickEffectDisplayTimer();
}


void Smash_It::setPoints(qint32 points)
{
    m_totalPoints = points;
    m_pointsDisplay->setPlainText("Points: " + QString::number(m_totalPoints));
    if(m_multiplayerModeActive){
        QJsonObject msg;
        msg["points"] = points;
        sendMove(msg, Invalid);
    }
}

void Smash_It::setDefaultGameParameters()
{
    m_totalPoints = 0;
    m_secondsLeft = 60;
    m_gameStarted = false;
    m_gameEnded = false;
    m_highScoreBeatenAnimation = false;
}

void Smash_It::activateClickEffectDisplayTimer()
{
    if(m_timerClickEffectDisplay!=nullptr){
        m_timerClickEffectDisplay->stop();
        delete m_timerClickEffectDisplay;
        m_timerClickEffectDisplay = nullptr;
    }
    m_timerClickEffectDisplay = new QTimer();
    connect(m_timerClickEffectDisplay, &QTimer::timeout, this, &Smash_It::removeClickEffectDisplay);
    m_timerClickEffectDisplay->setSingleShot(true);
    m_timerClickEffectDisplay->start(250);
}

void Smash_It::highScoreBeatenAnimation()
{
    m_highScoreBeatenAnimation = true;
    m_currentAntiDiagonal = 0;
    m_currentSide=true;
    m_highScoreBeatenAnimationTimer = new QTimer();

    // zelimo da m_highScoreBeatenAnimationTimer na svakih 50 milisekundi aktivira
    // forceChangeAlongTheDirection koji ce drzati status polja 100 milisekundi
    // sa statusom NORMAL_ACTIVE
    connect(m_highScoreBeatenAnimationTimer, &QTimer::timeout, this, ([this](){
        emit forceChangeAlongTheDirection(Direction::ANTI_DIAGONAL, m_currentAntiDiagonal,
                                          Status::NORMAL_ACTIVE, 100);
        // m_currentSide odredjuje da li animacija ide ka levo ili ka desno
        // kada je true idemo desno, kada se promeni na false znaci da se vracamo tj idemo levo
        if(m_currentSide == true){
            m_currentAntiDiagonal++;
            if(m_currentAntiDiagonal > m_antiDiagonalLimit){
                m_currentSide = false;
                m_currentAntiDiagonal = m_currentAntiDiagonal-1;
            }
        }
        else{
            m_currentAntiDiagonal--;
            if(m_currentAntiDiagonal == -1){
                m_highScoreBeatenAnimationTimer->stop();
                delete m_highScoreBeatenAnimationTimer;
                m_highScoreBeatenAnimationTimer=nullptr;
                m_highScoreBeatenAnimation = false;
                if(m_multiplayerModeActive == true){
                    // ako je aktiviran multiplayer mod treba da se vratimo
                    // u funkciju onNewGameOffer, ali ce zastavica m_winningAnimationStarted
                    // sada biti true pa se animacija nece opet pokretati
                    onNewGameOffer();
                }
            }
        }
    }
    ));

    m_highScoreBeatenAnimationTimer->start(50);
}

void Smash_It::removeClickEffectDisplay()
{
    delete m_timerClickEffectDisplay;
    m_timerClickEffectDisplay=nullptr;
    m_clickEffectDisplay->setPlainText("");
}

void Smash_It::unavailableServer()
{
    scene()->clear();
    m_exitButton = new Smash_ItButton(25,0,100, 50, "exit");
    scene()->addItem(m_exitButton);

    m_returnToMenuButton = new Smash_ItButton(-125,0,100, 50, "menu");
    scene()->addItem(m_returnToMenuButton);
    connect(m_returnToMenuButton, &Smash_ItButton::returnToMenu,
            this, &Smash_It::returnToMenu);

    auto serverUnavailableMsg = scene()->addText("server unavailable");
    serverUnavailableMsg->moveBy( -180, -scene()->height()/3+80);
    serverUnavailableMsg->setScale(3);
}


bool Smash_It::getGameEnded() const
{
    return m_gameEnded;
}

void Smash_It::decreaseSecondsLeft()
{
    decreaseSecondsLeft(1);
}

void Smash_It::decreaseSecondsLeft(qint32 seconds){
    m_secondsLeft -= seconds;
    m_secondsLeftDisplay->setPlainText("Time left: " + QString::number(m_secondsLeft));
    if(m_secondsLeft<=0){
        // ako je broj sekundi 0, ili manje (moguce samo u single player varijanti igre,
        // kada se na manje od 10 sekundi do kraja pritisne dugme za umanjenje 10 sekundi)
        // prekida se igra
        endGame();
        if(m_multiplayerModeActive == false){
            m_startGameButton->setText("restart");
        }
        return;
    }
    if(m_multiplayerModeActive == true){
        // saljemo informaciju da smo zavrsili
        QJsonObject msg;
        msg["seconds"] = m_secondsLeft;
        sendMove(msg, Invalid);
    }
    // ne zelimo da nam se desno od broja poena prikazuje informacija da se
    // broj sekundi ,,redovno" smanjio, tj to zelimo da prikazemo samo ako
    // je igrac pritisnuo da poveca ili smanji vreme
    if(seconds == 1)
        return;
    m_clickEffectDisplay->setDefaultTextColor(QColor(210,40,75));
    m_clickEffectDisplay->setPlainText("-" + QString::number(seconds) + " seconds");

    activateClickEffectDisplayTimer();
}

void Smash_It::increaseSecondsLeft(qint32 seconds)
{
    decreaseSecondsLeft(-seconds);
    m_clickEffectDisplay->setDefaultTextColor(QColor(50,205,50));
    m_clickEffectDisplay->setPlainText("+" + QString::number(seconds) + " seconds");
    activateClickEffectDisplayTimer();
}

void Smash_It::connectedToServer()
{
    /*
     * Pokrece se kada se povezemo sa serverom.
    */

    initNewGame();

    m_multiplayerModeActive = true;
    setWaitingScene();
}

void Smash_It::disconnectedFromServer()
{
    /*
     * Metod ce biti pozvan ukoliko dodje do
     * "regularnog" prekida konekcije
     * (mi prekidamo vezu, server se ugasio i sl).
    */
    if(m_multiplayerModeActive == true){
        returnToMenu();
        showMessage("Disconnected from server");
    }
}

void Smash_It::gameInit(int player, int gameIndex)
{
    /*
     * Vec cekamo u redu za igru, server je uspeo
     * da nas upari sa nekim igracem.
     * Odredio je ko je koji igrac.
     *
     * Ulazni argumenti sluze samo da se proslede
     * verziji funkcije od nadklase, ona sve podesava.
    */

    Game::gameInit(player, gameIndex);

    setGameScene();
}

void Smash_It::opponentMove(const QJsonObject &move)
{
    /*
     * Drugi igrac nam je prosledio svoj potez,
     * ovde reagujemo na njega.
    */

    /*
     * Napomena:
     * Poruka ne sme da sadrzi polja 'type', 'outcome', 'gameIndex'.
     * To su kontrolni podaci i podesice se interno pozivom sendMove.
    */
    if(move.contains("points"))
        setOpponentsPoints(move["points"].toInt());
    if(move.contains("seconds")){
        if(move["seconds"].toInt()<=0){
            m_opponentFinished = true;
        }
    }
    if(m_gameEnded == true && m_opponentFinished == true){
        QJsonObject msg;
        sendMove(msg, Player1);
    }
}

void Smash_It::onNewGameOffer()
{
    /*
     * Igra se je zavrsila na regularan nacin.
     * Server nas je stavio u opstu listu klijenata
     * i pita nas da li zelimo da igramo opet.
    */

    // igrac koji je pobedio pokrece animaciju za pobedu
    if(m_totalPoints > m_opponentsPoints){
        if(m_winningAnimationStarted == false){
            m_winningAnimationStarted = true;
            scene()->removeItem(m_opponentsPointsDisplay);
            auto winMessageDisplay = scene()->addText(
                        QString::fromStdString("YOU WON!"));
            winMessageDisplay->moveBy( -175, +scene()->height()/3+50);
            winMessageDisplay->setScale(2);
            highScoreBeatenAnimation();
            return;
        }
    }

    // igrac se obavestava o ishodu i rezultatu igre
    QString temp = "";
    if(m_totalPoints > m_opponentsPoints)
        temp += "You won ";
    else if(m_totalPoints < m_opponentsPoints)
        temp += "You lost ";
    else
        temp += "Draw ";
    temp += QString::number(m_totalPoints);
    temp += ":";
    temp += QString::number(m_opponentsPoints);

    showMessage(temp);

    setNewGameScene();

}

void Smash_It::onGameInterrupted()
{
    /*
     * Igra je bila u toku i drugi igrac je
     * prekinuo vezu. Server nas je vratio u red za
     * cekanje i obavestava nas o tome.
    */

    returnToMenu();
    showMessage("opponent disconnected");
}

void Smash_It::onConnectionError(QAbstractSocket::SocketError socketError)
{
    /*
     * Ovde reagujemo na greske sa konekcijom
     * koje su u grupi "neregularnih".
     *
     * Obavezno prvo pozvati metod error iz nadklase
     * i proslediti mu argument. On ce generisati poruku
     * o gresci koju mozemo dobiti getter metodom ispod.
     *
     * Napomena: cak i kada npr. mi sami prekinemo vezu,
     * to se smatra greskom. Medjutim, funkcija nadklase
     * ce u takvim "regularnim" slucajevima postaviti errorMessage
     * na nullptr - stoga provera toga ispod.
     *
     * Ovakvi slucajevi, kao i kada mi sami prekinemo vezu,
     * obradjuju se u slotu disconnectedFromServer.
    */

    Game::error(socketError);
    QString errorMsg = errorMessage();
    if(errorMsg == nullptr) {
        return;
    }

    returnToMenu();
    showMessage("connection error");
}
