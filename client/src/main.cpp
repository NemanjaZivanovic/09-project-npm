#include <QApplication>
#include <QIcon>
#include <headers/view.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    View w;
    w.setWindowIcon(QIcon((":/assets/images/icon.png")));
    w.setWindowTitle(QString("Project NPM"));
    w.show();
    return a.exec();
}
