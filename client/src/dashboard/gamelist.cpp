#include <headers/dashboard/gameitem.h>
#include <headers/dashboard/gamelist.h>
#include <headers/dashboard/gamedescriptionbox.h>

#include <QDebug>
#include <QString>
#include <QVector>
#include <QGraphicsView>

GameList::GameList(qreal gameItemSize, size_t n, qreal width, qreal height)
{
    m_gameItemSize = gameItemSize;
    m_gameNum = n;
    m_step = 5*gameItemSize/4;

    m_width = width;
    m_height = height;

    // Postavlja se okvir liste:
    // Levi kraj okvira popunjava prvi element, ali se uzimaju u obzir
    // uvecane dimenzije pri hoveru (3*size/2)

    const qreal sizeHover = 3*m_gameItemSize/2;
    m_rect = QRectF(
                width/2 -m_step - sizeHover/2,
                height/2 -sizeHover/2,
                m_gameItemSize * m_gameNum + (m_gameItemSize/4) * (m_gameNum+1),
                sizeHover
    );

    // Granicne vrednosti za skrolovanje
    m_maxX = m_nextX = 0;
    m_minX = -m_step * (m_gameNum-3);

    QImage gameImages[] = {
        QImage(":/assets/images/games/connect4/connect4.png"),
        QImage(":/assets/images/games/sibicarenje/sibicarenjePreview.png"),
        QImage(":/assets/images/placeholder.png"),
        QImage(":/assets/images/games/gridpolitics/gridpolitics.png"),
        QImage(":/assets/images/games/smash-it/smash-itPreview.png"),
        QImage(":/assets/images/placeholder.png")
    };

    auto rect = QRectF(-m_gameItemSize/2, -m_gameItemSize/2, m_gameItemSize, m_gameItemSize);
    auto maxRect = QRectF(rect.x() - m_gameItemSize/4, rect.y() - m_gameItemSize/4,
                       rect.width() + m_gameItemSize/2, rect.height() + m_gameItemSize/2);

    auto gameDescription = new GameDescriptionBox(maxRect);
    // Svakom GameItem objektu se postavlja GameList kao roditelj i pozicionira u odnosu na njega
    QVector<GameItem*> games(m_gameNum);
    for(auto i=0; i<games.size(); i++) {

        GameItem::GameId gameId = GameItem::GameId(i);
        games[i] = new GameItem(gameId, m_gameItemSize, gameImages[i], gameDescription);

        games[i]->setParentItem(this);
        games[i]->setPos(m_width/2 - m_step + i*m_step, m_height/2);
    }
}

QRectF GameList::boundingRect() const
{
    return m_rect;
}

void GameList::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
//    painter->drawRect(m_rect);
    Q_UNUSED(painter);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

//flag: true=desno, false=levo
// funkcija koja koristi vec implementirani wheelEvent
void GameList::callWheel(bool flag)
{
    QGraphicsSceneWheelEvent* event = new QGraphicsSceneWheelEvent();
    // odredjivanje koji ,,skrol" je u pitanju, posto ovo pozivamo kada se pritisne dugme
    // setDelta je negativno ako je skrol u desno, dakle treba staviti nesto negativno
    // suprotan slucaj je levo, a posto je false levo, -0 je 0 pa odgovara
    event->setDelta(-flag);
    emit wheelEvent(event);
}

void GameList::resetGameListPosition()
{
    //dok nam pozicija nije pocetna, skroluj na levo
    while(m_nextX!=m_maxX)
        callWheel(false);
}

void GameList::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    QGraphicsItem::wheelEvent(event);

    if(event->delta() < 0) {

        // Stopira se skrol u desno ako je aktivan
        if(m_timerRight) {
            m_timerRight->stop();

            m_timerRight->deleteLater();
            m_timerRight = nullptr;
        }

        // 1. varijanta - nije aktivno nikakvo skrolovanje - novi tajmer
        // 2. varijanta - upravo je stopirano skrolovanje u desno - novi tajmer i stop skrola u desno
        // 3. varijanta - aktivno je skrolovanje u levo, treba mu samo produziti granicu
        if(m_timerLeft && m_timerLeft->isActive()) {
            // 3. varijanta
            if(m_nextX - m_step >= m_minX) {
               m_nextX -= m_step;
            }
            event->accept();
            return;
        }

        // novi tajmer osim ako smo vec skroz levo
        if(m_nextX > m_minX) {
            m_nextX -= m_step;

            m_timerLeft = new QTimer(this);
            connect(m_timerLeft, SIGNAL(timeout()), this, SLOT(scrollLeft()));
            m_timerLeft->start(1000/120);
        }
    } else {

        // Sve analogno i za drugu stranu
        if(m_timerLeft) {
            m_timerLeft->stop();

            m_timerLeft->deleteLater();
            m_timerLeft = nullptr;
        }

        if(m_timerRight && m_timerRight->isActive()) {
            // 3. varijanta
            if(m_nextX + m_step <= m_maxX) {
               m_nextX += m_step;
            }
            event->accept();
            return;
        }

        if(m_nextX < m_maxX) {
            m_nextX += m_step;

            m_timerRight = new QTimer(this);
            connect(m_timerRight, SIGNAL(timeout()), this, SLOT(scrollRight()));
            m_timerRight->start(1000/120);
        }
    }

    event->accept();

}

void GameList::scrollRight()
{
    const qreal step = 5;

    if(pos().x() < m_nextX) {
        setPos(pos().x() + step, pos().y());

    } else {
        setPos(m_nextX, pos().y());
        m_timerRight->stop();

        m_timerRight->deleteLater();
        m_timerRight = nullptr;
    }
}

void GameList::scrollLeft()
{
    const qreal step = 5;

    if(pos().x() > m_nextX) {
        setPos(pos().x() - step, pos().y());
    } else {
        setPos(m_nextX, pos().y());
        m_timerLeft->stop();

        m_timerLeft->deleteLater();
        m_timerLeft = nullptr;
    }
}


