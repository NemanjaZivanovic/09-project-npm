#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QAbstractSocket>
#include <QHostAddress>
#include <QObject>
#include <QTcpSocket>
#include <QJsonObject>

class GameClient : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameClient)

public:
    explicit GameClient(QObject *parent = nullptr);

    void disconnect();
    bool isConnected();
    void sendJson(const QJsonObject &jsonObj);

public slots:
    void connectToServer(const QHostAddress &address, quint16 port);

signals:
    void connected();
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);
    void gameInit(int player, int gameIndex);
    void opponentMove(int move);
    void newGameOffer();
    void gameInterrupted();

private slots:
    void onReadyRead();

private:
    void jsonReceived(const QJsonObject &jsonObj);

private:
    QTcpSocket* m_clientSocket;
};

#endif // GAMECLIENT_H
